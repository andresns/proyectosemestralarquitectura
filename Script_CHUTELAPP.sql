/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     18/11/2018 16:05:45                          */
/*==============================================================*/


alter table CANCHA 
   drop foreign key FK_CANCHA_RELATIONS_COMUNA;

alter table COMUNA 
   drop foreign key FK_COMUNA_RELATIONS_REGION;

alter table DISPONIBILIDAD 
   drop foreign key FK_DISPONIB_RELATIONS_JUGADOR;

alter table DISPONIBILIDAD 
   drop foreign key FK_DISPONIB_RELATIONS_DIA;

alter table DISPONIBILIDAD 
   drop foreign key FK_DISPONIB_RELATIONS_CANCHA;

alter table EQUIPO 
   drop foreign key FK_EQUIPO_RELATIONS_COMUNA;

alter table EVALUACION_CABECERA 
   drop foreign key FK_EVALUACI_RELATIONS_JUGADOR;

alter table EVALUACION_DETALLE 
   drop foreign key FK_EVALUACI_RELATIONS_EVALUACI;

alter table EVALUACION_DETALLE 
   drop foreign key FK_EVALUACI_RELATIONS_JUGADOR;

alter table EVALUACION_DETALLE 
   drop foreign key FK_EVALUACI_RELATIONS_ESCALA;

alter table EVALUACION_DETALLE 
   drop foreign key FK_EVALUACI_RELATIONS_CANCHA;

alter table EVALUACION_DETALLE 
   drop foreign key FK_EVALUACI_RELATIONS_EQUIPO;

alter table JUGADOR 
   drop foreign key FK_JUGADOR_RELATIONS_POSICION;

alter table JUGADOR 
   drop foreign key FK_JUGADOR_RELATIONS_COMUNA;

alter table JUGADOR_EQUIPO 
   drop foreign key FK_JUGADOR__RELATIONS_JUGADOR;

alter table JUGADOR_EQUIPO 
   drop foreign key FK_JUGADOR__RELATIONS_EQUIPO;

alter table PARTIDO 
   drop foreign key FK_PARTIDO_RELATIONS_TIPO_PAR;

alter table PARTIDO 
   drop foreign key FK_PARTIDO_RELATIONS_EQUIPO;

alter table PARTIDO 
   drop foreign key FK_PARTIDO_RELATIONS_EQUIPO;

alter table PARTIDO 
   drop foreign key FK_PARTIDO_RELATIONS_CANCHA;

alter table SOLICITUD 
   drop foreign key FK_SOLICITU_RELATIONS_JUGADOR;

alter table SOLICITUD 
   drop foreign key FK_SOLICITU_RELATIONS_JUGADOR;

alter table SOLICITUD 
   drop foreign key FK_SOLICITU_RELATIONS_EQUIPO;

alter table SOLICITUD 
   drop foreign key FK_SOLICITU_RELATIONS_EQUIPO;


alter table CANCHA 
   drop foreign key FK_CANCHA_RELATIONS_COMUNA;

drop table if exists CANCHA;


alter table COMUNA 
   drop foreign key FK_COMUNA_RELATIONS_REGION;

drop table if exists COMUNA;

drop table if exists DIA;


alter table DISPONIBILIDAD 
   drop foreign key FK_DISPONIB_RELATIONS_JUGADOR;

alter table DISPONIBILIDAD 
   drop foreign key FK_DISPONIB_RELATIONS_DIA;

alter table DISPONIBILIDAD 
   drop foreign key FK_DISPONIB_RELATIONS_CANCHA;

drop table if exists DISPONIBILIDAD;


alter table EQUIPO 
   drop foreign key FK_EQUIPO_RELATIONS_COMUNA;

drop table if exists EQUIPO;

drop table if exists ESCALA;


alter table EVALUACION_CABECERA 
   drop foreign key FK_EVALUACI_RELATIONS_JUGADOR;

drop table if exists EVALUACION_CABECERA;


alter table EVALUACION_DETALLE 
   drop foreign key FK_EVALUACI_RELATIONS_CANCHA;

alter table EVALUACION_DETALLE 
   drop foreign key FK_EVALUACI_RELATIONS_EQUIPO;

alter table EVALUACION_DETALLE 
   drop foreign key FK_EVALUACI_RELATIONS_EVALUACI;

alter table EVALUACION_DETALLE 
   drop foreign key FK_EVALUACI_RELATIONS_JUGADOR;

alter table EVALUACION_DETALLE 
   drop foreign key FK_EVALUACI_RELATIONS_ESCALA;

drop table if exists EVALUACION_DETALLE;


alter table JUGADOR 
   drop foreign key FK_JUGADOR_RELATIONS_POSICION;

alter table JUGADOR 
   drop foreign key FK_JUGADOR_RELATIONS_COMUNA;

drop table if exists JUGADOR;


alter table JUGADOR_EQUIPO 
   drop foreign key FK_JUGADOR__RELATIONS_JUGADOR;

alter table JUGADOR_EQUIPO 
   drop foreign key FK_JUGADOR__RELATIONS_EQUIPO;

drop table if exists JUGADOR_EQUIPO;


alter table PARTIDO 
   drop foreign key FK_PARTIDO_RELATIONS_CANCHA;

alter table PARTIDO 
   drop foreign key FK_PARTIDO_RELATIONS_TIPO_PAR;

alter table PARTIDO 
   drop foreign key FK_PARTIDO_RELATIONS_EQUIPO;

alter table PARTIDO 
   drop foreign key FK_PARTIDO_RELATIONS_EQUIPO;

drop table if exists PARTIDO;

drop table if exists POSICION;

drop table if exists REGION;


alter table SOLICITUD 
   drop foreign key FK_SOLICITU_RELATIONS_JUGADOR;

alter table SOLICITUD 
   drop foreign key FK_SOLICITU_RELATIONS_JUGADOR;

alter table SOLICITUD 
   drop foreign key FK_SOLICITU_RELATIONS_EQUIPO;

alter table SOLICITUD 
   drop foreign key FK_SOLICITU_RELATIONS_EQUIPO;

drop table if exists SOLICITUD;

drop table if exists TIPO_PARTIDO;

/*==============================================================*/
/* Table: CANCHA                                                */
/*==============================================================*/
create table CANCHA
(
   ID_CANCHA            numeric(10,0) not null AUTO_INCREMENT comment '',
   CODIGO_COMUNA        numeric(10,0) not null  comment '',
   NOMBRE_CANCHA        char(50)  comment '',
   FONO_CANCHA          char(15)  comment '',
   PRECIO_CANCHA        numeric(20,0)  comment '',
   DIRECCION            char(100)  comment '',
   primary key (ID_CANCHA)
);

/*==============================================================*/
/* Table: COMUNA                                                */
/*==============================================================*/
create table COMUNA
(
   CODIGO_COMUNA        numeric(10,0) not null  comment '',
   CODIGO_REGION        numeric(10,0) not null  comment '',
   NOMBRE_COMUNA        char(50)  comment '',
   primary key (CODIGO_COMUNA)
);

/*==============================================================*/
/* Table: DIA                                                   */
/*==============================================================*/
create table DIA
(
   ID_DIA               numeric(10,0) not null  comment '',
   NOMBRE_DIA           char(50)  comment '',
   primary key (ID_DIA)
);

/*==============================================================*/
/* Table: DISPONIBILIDAD                                        */
/*==============================================================*/
create table DISPONIBILIDAD
(
   ID_DISPONIBILIDAD    numeric(10,0) not null AUTO_INCREMENT comment '',
   ID_JUGADOR           numeric(10,0)  comment '',
   ID_DIA               numeric(10,0) not null  comment '',
   ID_CANCHA            numeric(10,0)  comment '',
   HORA_INICIO          date  comment '',
   HORA_TERMINO         date  comment '',
   primary key (ID_DISPONIBILIDAD)
);

/*==============================================================*/
/* Table: EQUIPO                                                */
/*==============================================================*/
create table EQUIPO
(
   ID_EQUIPO            numeric(10,0) not null AUTO_INCREMENT comment '',
   CODIGO_COMUNA        numeric(10,0) not null  comment '',
   NOMBRE_EQUIPO        char(50)  comment '',
   NOMNBRE_CONTACTO     char(100)  comment '',
   FONO_CONTACTO        char(15)  comment '',
   EMAIL_CONTACTO       char(50)  comment '',
   primary key (ID_EQUIPO)
);

/*==============================================================*/
/* Table: ESCALA                                                */
/*==============================================================*/
create table ESCALA
(
   ID_ESCALA           numeric(10,0) not null  comment '',
   VALORACION           numeric(2,0)  comment '',
   primary key (ID_ESCALA)
);

/*==============================================================*/
/* Table: EVALUACION_CABECERA                                   */
/*==============================================================*/
create table EVALUACION_CABECERA
(
   ID_EVALUACION_CABECERA numeric(10,0) not null AUTO_INCREMENT comment '',
   ID_JUGADOR           numeric(10,0) not null  comment '',
   primary key (ID_EVALUACION_CABECERA)
);

/*==============================================================*/
/* Table: EVALUACION_DETALLE                                    */
/*==============================================================*/
create table EVALUACION_DETALLE
(
   ID_EVALUACION_DETALLE numeric(10,0) not null AUTO_INCREMENT comment '',
   ID_CANCHA            numeric(10,0)  comment '',
   ID_EQUIPO            numeric(10,0) not null  comment '',
   ID_EVALUACION_CABECERA numeric(10,0) not null  comment '',
   ID_JUGADOR           numeric(10,0)  comment '',
   ID_ESCALA           numeric(10,0) not null  comment '',
   primary key (ID_EVALUACION_DETALLE)
);

/*==============================================================*/
/* Table: JUGADOR                                               */
/*==============================================================*/
create table JUGADOR
(
   ID_JUGADOR           numeric(10,0) not null AUTO_INCREMENT comment '',
   ID_POISICION         numeric(10,0) not null  comment '',
   CODIGO_COMUNA        numeric(10,0) not null  comment '',
   NOMBRES              char(100)  comment '',
   APELLIDOS            char(100)  comment '',
   NOMBRE_USUARIO       char(15)  comment '',
   CONTRASENA_USUARIO   char(30)  comment '',
   EMAIL                char(50)  comment '',
   FECHA_NACIMIENTO     datetime  comment '',
   TELEFONO             char(15)  comment '',
   primary key (ID_JUGADOR)
);

/*==============================================================*/
/* Table: JUGADOR_EQUIPO                                        */
/*==============================================================*/
create table JUGADOR_EQUIPO
(
   ID_JUGADOR           numeric(10,0) not null  comment '',
   ID_EQUIPO            numeric(10,0) not null  comment '',
   primary key (ID_JUGADOR, ID_EQUIPO)
);

/*==============================================================*/
/* Table: PARTIDO                                               */
/*==============================================================*/
create table PARTIDO
(
   ID_PARTIDO           numeric(10,0) not null AUTO_INCREMENT comment '',
   ID_CANCHA            numeric(10,0) not null  comment '',
   ID_TIPO_PARTIDO      numeric(10,0) not null  comment '',
   ID_EQUIPO            numeric(10,0)  comment '',
   EQU_ID_EQUIPO        numeric(10,0)  comment '',
   primary key (ID_PARTIDO)
);

/*==============================================================*/
/* Table: POSICION                                              */
/*==============================================================*/
create table POSICION
(
   ID_POISICION         numeric(10,0) not null  comment '',
   NOMBRE_POSICION      char(50)  comment '',
   primary key (ID_POISICION)
);

/*==============================================================*/
/* Table: REGION                                                */
/*==============================================================*/
create table REGION
(
   CODIGO_REGION        numeric(10,0) not null  comment '',
   NOMBRE_REGION        char(50)  comment '',
   primary key (CODIGO_REGION)
);

/*==============================================================*/
/* Table: SOLICITUD                                             */
/*==============================================================*/
create table SOLICITUD
(
   ID_SOLICITUD         numeric(10,0) not null AUTO_INCREMENT comment '',
   ID_JUGADOR           numeric(10,0)  comment '',
   JUG_ID_JUGADOR       numeric(10,0)  comment '',
   ID_EQUIPO            numeric(10,0)  comment '',
   EQU_ID_EQUIPO        numeric(10,0)  comment '',
   primary key (ID_SOLICITUD)
);

/*==============================================================*/
/* Table: TIPO_PARTIDO                                          */
/*==============================================================*/
create table TIPO_PARTIDO
(
   ID_TIPO_PARTIDO      numeric(10,0) not null  comment '',
   DESCRIPCION          char(50)  comment '',
   primary key (ID_TIPO_PARTIDO)
);

alter table CANCHA add constraint FK_CANCHA_RELATIONS_COMUNA foreign key (CODIGO_COMUNA)
      references COMUNA (CODIGO_COMUNA) on delete restrict on update restrict;

alter table COMUNA add constraint FK_COMUNA_RELATIONS_REGION foreign key (CODIGO_REGION)
      references REGION (CODIGO_REGION) on delete restrict on update restrict;

alter table DISPONIBILIDAD add constraint FK_DISPONIB_RELATIONS_JUGADOR foreign key (ID_JUGADOR)
      references JUGADOR (ID_JUGADOR) on delete restrict on update restrict;

alter table DISPONIBILIDAD add constraint FK_DISPONIB_RELATIONS_DIA foreign key (ID_DIA)
      references DIA (ID_DIA) on delete restrict on update restrict;

alter table DISPONIBILIDAD add constraint FK_DISPONIB_RELATIONS_CANCHA foreign key (ID_CANCHA)
      references CANCHA (ID_CANCHA) on delete restrict on update restrict;

alter table EQUIPO add constraint FK_EQUIPO_RELATIONS_COMUNA foreign key (CODIGO_COMUNA)
      references COMUNA (CODIGO_COMUNA) on delete restrict on update restrict;

alter table EVALUACION_CABECERA add constraint FK_EVALUACI_RELATIONS_JUGADOR foreign key (ID_JUGADOR)
      references JUGADOR (ID_JUGADOR) on delete restrict on update restrict;

alter table EVALUACION_DETALLE add constraint FK_EVALUACI_RELATIONS_EVALUACI foreign key (ID_EVALUACION_CABECERA)
      references EVALUACION_CABECERA (ID_EVALUACION_CABECERA) on delete restrict on update restrict;

alter table EVALUACION_DETALLE add constraint FK_EVALUACI_RELATIONS_JUGADOR foreign key (ID_JUGADOR)
      references JUGADOR (ID_JUGADOR) on delete restrict on update restrict;

alter table EVALUACION_DETALLE add constraint FK_EVALUACI_RELATIONS_ESCALA foreign key (ID_ESCALA)
      references ESCALA (ID_ESCALA) on delete restrict on update restrict;

alter table EVALUACION_DETALLE add constraint FK_EVALUACI_RELATIONS_CANCHA foreign key (ID_CANCHA)
      references CANCHA (ID_CANCHA) on delete restrict on update restrict;

alter table EVALUACION_DETALLE add constraint FK_EVALUACI_RELATIONS_EQUIPO foreign key (ID_EQUIPO)
      references EQUIPO (ID_EQUIPO) on delete restrict on update restrict;

alter table JUGADOR add constraint FK_JUGADOR_RELATIONS_POSICION foreign key (ID_POISICION)
      references POSICION (ID_POISICION) on delete restrict on update restrict;

alter table JUGADOR add constraint FK_JUGADOR_RELATIONS_COMUNA foreign key (CODIGO_COMUNA)
      references COMUNA (CODIGO_COMUNA) on delete restrict on update restrict;

alter table JUGADOR_EQUIPO add constraint FK_JUGADOR__RELATIONS_JUGADOR foreign key (ID_JUGADOR)
      references JUGADOR (ID_JUGADOR) on delete restrict on update restrict;

alter table JUGADOR_EQUIPO add constraint FK_JUGADOR__RELATIONS_EQUIPO foreign key (ID_EQUIPO)
      references EQUIPO (ID_EQUIPO) on delete restrict on update restrict;

alter table PARTIDO add constraint FK_PARTIDO_RELATIONS_TIPO_PAR foreign key (ID_TIPO_PARTIDO)
      references TIPO_PARTIDO (ID_TIPO_PARTIDO) on delete restrict on update restrict;

alter table PARTIDO add constraint FK_PARTIDO_RELATIONS_EQUIPO foreign key (ID_EQUIPO)
      references EQUIPO (ID_EQUIPO) on delete restrict on update restrict;

alter table PARTIDO add constraint FK_PARTIDO_RELATIONS_EQUIPO foreign key (EQU_ID_EQUIPO)
      references EQUIPO (ID_EQUIPO) on delete restrict on update restrict;

alter table PARTIDO add constraint FK_PARTIDO_RELATIONS_CANCHA foreign key (ID_CANCHA)
      references CANCHA (ID_CANCHA) on delete restrict on update restrict;

alter table SOLICITUD add constraint FK_SOLICITU_RELATIONS_JUGADOR foreign key (ID_JUGADOR)
      references JUGADOR (ID_JUGADOR) on delete restrict on update restrict;

alter table SOLICITUD add constraint FK_SOLICITU_RELATIONS_JUGADOR foreign key (JUG_ID_JUGADOR)
      references JUGADOR (ID_JUGADOR) on delete restrict on update restrict;

alter table SOLICITUD add constraint FK_SOLICITU_RELATIONS_EQUIPO foreign key (ID_EQUIPO)
      references EQUIPO (ID_EQUIPO) on delete restrict on update restrict;

alter table SOLICITUD add constraint FK_SOLICITU_RELATIONS_EQUIPO foreign key (EQU_ID_EQUIPO)
      references EQUIPO (ID_EQUIPO) on delete restrict on update restrict;

