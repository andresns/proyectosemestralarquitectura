/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CapaNegocio;

import CapaConexion.ConexionMySql;
import CapaDTO.Partido;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Maltra
 */
public class NegocioPartido {
    
    private ConexionMySql conec = new ConexionMySql();
    
    
    public void configurarConexion()
    {
       this.getConec().setNombreBaseDatos("jdbc:mysql://localhost/chutealapp");
       this.getConec().setNombreTabla("partido");
       this.getConec().setCadenaConexion("com.mysql.jdbc.Driver");
       this.getConec().setUsuario("root");
       this.getConec().setPass("");
    }


    public void insertarPartido(Partido partido)
    {
        this.configurarConexion();
        this.getConec().setCadenaSQL("INSERT INTO " + this.getConec().getNombreTabla()+
                                     " (id_partido,"
                                    + "id_cancha,"
                                    + "id_tipo_partido,"
                                    + "id_equipo,"
                                    + "equ_id_equipo) VALUES "
                                    + "(null,"
                                    +partido.getId_cancha()+ ","
                                    +partido.getId_tipo_partido()+ ","
                                    +partido.getId_equipo_1()+ ","
                                    +partido.getId_equipo_2()+ ");");
        this.getConec().setEsSelect(false);
        this.getConec().conectar();
    }
    
    public ArrayList<Partido> listarPartudos()
    {
        ArrayList<Partido> auxListadoPartidos= new ArrayList<>();
        this.configurarConexion();
        this.getConec().setCadenaSQL("SELECT * FROM " + this.getConec().getNombreTabla() + ";");
        this.getConec().setEsSelect(true);
        this.getConec().conectar();
        
        try
        {
           while(this.getConec().getDbResultSet().next())
           {
               Partido auxPartido = new Partido();
               auxPartido.setId_partido(this.getConec().getDbResultSet().getInt("id_partido"));
               auxPartido.setId_cancha(this.getConec().getDbResultSet().getInt("id_cancha"));
               auxPartido.setId_partido(this.getConec().getDbResultSet().getInt("id_tipo_partido"));
               auxPartido.setId_equipo_1(this.getConec().getDbResultSet().getInt("id_equipo"));
               auxPartido.setId_equipo_2(this.getConec().getDbResultSet().getInt("equ_id_equipo"));
               
               auxListadoPartidos.add(auxPartido); 
           }
        }
        catch(Exception ex)
        {
           JOptionPane.showMessageDialog(null,"Error comando SQL " + ex.getMessage());
        }
        return auxListadoPartidos;
    }

    public Partido buscarPartido(int idEquipo)
    {
        Partido auxPartido = new Partido();
        this.configurarConexion();
        this.getConec().setCadenaSQL("SELECT * FROM " + this.getConec().getNombreTabla()+
                                     " WHERE id_partido = " +idEquipo +";" );
        this.getConec().setEsSelect(true);
        this.getConec().conectar();
        
        try
        {
           if(this.getConec().getDbResultSet().next())
           {
               auxPartido.setId_partido(this.getConec().getDbResultSet().getInt("id_partido"));
               auxPartido.setId_cancha(this.getConec().getDbResultSet().getInt("id_cancha"));
               auxPartido.setId_partido(this.getConec().getDbResultSet().getInt("id_tipo_partido"));
               auxPartido.setId_equipo_1(this.getConec().getDbResultSet().getInt("id_equipo"));
               auxPartido.setId_equipo_2(this.getConec().getDbResultSet().getInt("equ_id_equipo"));
           }else{
                auxPartido.setId_partido(0);
                auxPartido.setId_cancha(0);
                auxPartido.setId_partido(0);
                auxPartido.setId_equipo_1(0);
                auxPartido.setId_equipo_2(0);
           }
        }
        catch(Exception ex)
        {
            auxPartido.setId_partido(0);
            auxPartido.setId_cancha(0);
            auxPartido.setId_partido(0);
            auxPartido.setId_equipo_1(0);
            auxPartido.setId_equipo_2(0);
        }
        return auxPartido;
    }
    
    
    /**
     * @return the conec
     */
    public ConexionMySql getConec() {
        return conec;
    }

    /**
     * @param conec the conec to set
     */
    public void setConec(ConexionMySql conec) {
        this.conec = conec;
    }
    
    
}
