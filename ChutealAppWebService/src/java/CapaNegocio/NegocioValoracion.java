/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CapaNegocio;

import CapaConexion.ConexionMySql;
import CapaDTO.Valoracion;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Andres
 */
public class NegocioValoracion {
    private ConexionMySql conec = new ConexionMySql();
    
    public void configurarConexion()
    {
       this.getConec().setNombreBaseDatos("jdbc:mysql://localhost/chutealapp");
       this.getConec().setNombreTabla("escala");
       this.getConec().setCadenaConexion("com.mysql.jdbc.Driver");
       this.getConec().setUsuario("root");
       this.getConec().setPass("");
    }

    
    //Listar todos los equipo
    public ArrayList<Valoracion> listarValoraciones()
    {
        ArrayList<Valoracion> auxListadoValoraciones = new ArrayList<>();
        this.configurarConexion();
        this.getConec().setCadenaSQL("SELECT * FROM " + this.getConec().getNombreTabla() + ";");
        this.getConec().setEsSelect(true);
        this.getConec().conectar();
        
        try
        {
           while(this.getConec().getDbResultSet().next())
           {
               Valoracion auxComuna = new Valoracion();
               auxComuna.setId_escala(this.getConec().getDbResultSet().getInt("id_escala"));
               auxComuna.setValoracion(this.getConec().getDbResultSet().getDouble("valoracion"));


               auxListadoValoraciones.add(auxComuna);
               
           }
        
        }
        catch(Exception ex)
        {
           JOptionPane.showMessageDialog(null,"Error comando SQL " + ex.getMessage());
        }
        
        return auxListadoValoraciones;
    }
    
    public Valoracion buscarValoracion(int id_escala)
    {
        Valoracion auxValoracion = new Valoracion();
        this.configurarConexion();
        this.getConec().setCadenaSQL("SELECT * FROM " + this.getConec().getNombreTabla()+
                                     " WHERE id_escala = " +id_escala +";" );
        this.getConec().setEsSelect(true);
        this.getConec().conectar();
        
        try
        {
           if(this.getConec().getDbResultSet().next())
           {
               auxValoracion.setId_escala(this.getConec().getDbResultSet().getInt("id_escala"));
               auxValoracion.setValoracion(this.getConec().getDbResultSet().getDouble("valoracion"));
           }
           else{
                auxValoracion.setId_escala(0);
                auxValoracion.setValoracion(0);
           }
        
        }
        catch(Exception ex)
        {
            auxValoracion.setId_escala(0);
            auxValoracion.setValoracion(0);

        }
        
        return auxValoracion;
    }
    
    /**
     * @return the conec
     */
    public ConexionMySql getConec() {
        return conec;
    }

    /**
     * @param conec the conec to set
     */
    public void setConec(ConexionMySql conec) {
        this.conec = conec;
    }
}
