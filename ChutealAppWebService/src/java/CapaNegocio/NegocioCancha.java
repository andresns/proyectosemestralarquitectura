/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CapaNegocio;

import CapaConexion.ConexionMySql;
import CapaDTO.Cancha;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Andres
 */
public class NegocioCancha {
    
    private ConexionMySql conec = new ConexionMySql();
    
    public void configurarConexion()
    {
       this.getConec().setNombreBaseDatos("jdbc:mysql://localhost/chutealapp");
       this.getConec().setNombreTabla("cancha");
       this.getConec().setCadenaConexion("com.mysql.jdbc.Driver");
       this.getConec().setUsuario("root");
       this.getConec().setPass("");
    }
    
    public void insertarCancha(Cancha cancha)
    {
        this.configurarConexion();
        this.getConec().setCadenaSQL("INSERT INTO " + this.getConec().getNombreTabla()+
                                     " (id_cancha,"
                                    + "codigo_comuna,"
                                    + "nombre_cancha,"
                                    + "fono_cancha,"
                                    + "precio_cancha,"
                                    + "direccion) VALUES "
                                    + "(null,"
                                    +cancha.getId_comuna()+ ","
                                    + "'"+cancha.getNombre()+ "',"
                                    + "'"+cancha.getTelefono()+ "',"
                                    +cancha.getPrecio()+ ","
                                    + "'"+cancha.getDireccion()+"');");
        this.getConec().setEsSelect(false);
        this.getConec().conectar();
    }
    
    public void actualizarCancha(Cancha cancha)
    {
        this.configurarConexion();
        this.getConec().setCadenaSQL("UPDATE " + this.getConec().getNombreTabla()+
                                     " SET codigo_comuna = "+cancha.getId_comuna()+","
                                        + "nombre_cancha = '"+cancha.getNombre()+"',"
                                        + "fono_cancha = '"+cancha.getTelefono()+"',"
                                        + "precio_cancha = "+cancha.getPrecio()+","
                                        + "direccion = '"+cancha.getDireccion()+"'"
                                        + " WHERE id_cancha = "+  cancha.getId_cancha()+ ";");
        this.getConec().setEsSelect(false);
        this.getConec().conectar();
    }

    public void eliminarCancha(int id_cancha)
    {
        this.configurarConexion();
        this.getConec().setCadenaSQL("DELETE FROM " + this.getConec().getNombreTabla()+
                                     " WHERE id_cancha = "+  id_cancha + ";");
        this.getConec().setEsSelect(false);
        this.getConec().conectar();
    }
    
    public ArrayList<Cancha> listarCanchas()
    {
        ArrayList<Cancha> auxListaCanchas = new ArrayList<>();
        this.configurarConexion();
        this.getConec().setCadenaSQL("SELECT * FROM " + this.getConec().getNombreTabla() + ";");
        this.getConec().setEsSelect(true);
        this.getConec().conectar();
        
        try
        {
           while(this.getConec().getDbResultSet().next())
           {
               Cancha auxCancha = new Cancha();
               auxCancha.setId_cancha(this.getConec().getDbResultSet().getInt("id_cancha"));
               auxCancha.setId_comuna(this.getConec().getDbResultSet().getInt("codigo_comuna"));
               auxCancha.setNombre(this.getConec().getDbResultSet().getString("nombre_cancha"));
               auxCancha.setTelefono(this.getConec().getDbResultSet().getString("fono_cancha"));
               auxCancha.setPrecio(this.getConec().getDbResultSet().getInt("precio_cancha"));
               auxCancha.setDireccion(this.getConec().getDbResultSet().getString("direccion"));
               
               auxListaCanchas.add(auxCancha);
           }
        
        }
        catch(Exception ex)
        {
           JOptionPane.showMessageDialog(null,"Error comando SQL " + ex.getMessage());
        }
        
        return auxListaCanchas;
    }
    

    public Cancha buscarCancha(String nombre_cancha)
    {
        Cancha auxCancha = new Cancha();
        this.configurarConexion();
        this.getConec().setCadenaSQL("SELECT * FROM " + this.getConec().getNombreTabla()+
                                     " WHERE nombre_cancha = '" +nombre_cancha +"';" );
        this.getConec().setEsSelect(true);
        this.getConec().conectar();
        
        try
        {
           if(this.getConec().getDbResultSet().next())
           {
               auxCancha.setId_cancha(this.getConec().getDbResultSet().getInt("id_cancha"));
               auxCancha.setId_comuna(this.getConec().getDbResultSet().getInt("codigo_comuna"));
               auxCancha.setNombre(this.getConec().getDbResultSet().getString("nombre_cancha"));
               auxCancha.setTelefono(this.getConec().getDbResultSet().getString("fono_cancha"));
               auxCancha.setPrecio(this.getConec().getDbResultSet().getInt("precio_cancha"));
               auxCancha.setDireccion(this.getConec().getDbResultSet().getString("direccion"));
            
           }
           else{
                auxCancha.setId_cancha(0);
                auxCancha.setId_comuna(0);
                auxCancha.setNombre("");
                auxCancha.setTelefono("");
                auxCancha.setPrecio(0);
                auxCancha.setDireccion("");
           }
        
        }
        catch(Exception ex)
        {
            auxCancha.setId_cancha(0);
            auxCancha.setId_comuna(0);
            auxCancha.setNombre("");
            auxCancha.setTelefono("");
            auxCancha.setPrecio(0);
            auxCancha.setDireccion("");
        }
        
        return auxCancha;
    }
    
    /**
     * @return the conec
     */
    public ConexionMySql getConec() {
        return conec;
    }

    /**
     * @param conec the conec to set
     */
    public void setConec(ConexionMySql conec) {
        this.conec = conec;
    }
    
}
