/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CapaNegocio;

import CapaConexion.ConexionMySql;
import CapaDTO.Jugador;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JOptionPane;

/**
 *
 * @author Andres
 */
public class NegocioJugador {
    private ConexionMySql conec = new ConexionMySql();
    
    public void configurarConexion()
    {
       this.getConec().setNombreBaseDatos("jdbc:mysql://localhost/chutealapp");
       this.getConec().setNombreTabla("jugador");
       this.getConec().setCadenaConexion("com.mysql.jdbc.Driver");
       this.getConec().setUsuario("root");
       this.getConec().setPass("");
    }


    public void insertarJugador(Jugador jugador)
    {
        this.configurarConexion();
        this.getConec().setCadenaSQL("INSERT INTO " + this.getConec().getNombreTabla()+
                                     " (id_jugador,"
                                    + "nombres,"
                                    + "apellidos,"
                                    + "nombre_usuario,"
                                    + "contrasena_usuario,"
                                    + "email,"
                                    + "telefono,"
                                    + "fecha_nacimiento,"
                                    + "id_posicion,"
                                    + "codigo_comuna) VALUES "
                                    + "(null,"
                                    + "'"+jugador.getNombres()+ "',"
                                    + "'"+jugador.getApellidos()+ "',"
                                    + "'"+jugador.getNombre_usuario()+ "',"
                                    + "'"+jugador.getContrasena()+ "',"
                                    + "'"+jugador.getEmail()+ "',"
                                    + "'"+jugador.getTelefono()+ "',"
                                    +"'"+jugador.getFecha_nacimiento()+ "',"
                                    +jugador.getId_posicion()+ ","
                                    +jugador.getId_comuna()+ ");");
        this.getConec().setEsSelect(false);
        this.getConec().conectar();
    }
    
    public void actualizarJugador(Jugador jugador)
    {
        this.configurarConexion();
        this.getConec().setCadenaSQL("UPDATE " + this.getConec().getNombreTabla()+
                                     " SET nombres = '"+jugador.getNombres()+"',"
                                        + "apellidos = '"+jugador.getApellidos()+"',"
                                        + "codigo_comuna = "+jugador.getId_comuna()+","
                                        + "id_posicion = "+jugador.getId_posicion()+","
                                        + "telefono = '"+jugador.getTelefono()+"',"
                                        + "email = '"+jugador.getEmail()+"'"
                                        + " WHERE id_jugador = "+  jugador.getId_jugador()+ ";");
        this.getConec().setEsSelect(false);
        this.getConec().conectar();
    }

    public void eliminarJugador(int id_jugador)
    {
        this.configurarConexion();
        this.getConec().setCadenaSQL("DELETE FROM " + this.getConec().getNombreTabla()+
                                     " WHERE id_jugador = "+  id_jugador + ";");
        this.getConec().setEsSelect(false);
        this.getConec().conectar();
    }
    
    public ArrayList<Jugador> listarJugadores()
    {
        ArrayList<Jugador> auxListadoJugadores = new ArrayList<>();
        this.configurarConexion();
        this.getConec().setCadenaSQL("SELECT * FROM " + this.getConec().getNombreTabla() + ";");
        this.getConec().setEsSelect(true);
        this.getConec().conectar();
        
        try
        {
           while(this.getConec().getDbResultSet().next())
           {
               Jugador auxJugador = new Jugador();
               auxJugador.setId_jugador(this.getConec().getDbResultSet().getInt("id_jugador"));
               auxJugador.setNombres(this.getConec().getDbResultSet().getString("nombres"));
               auxJugador.setApellidos(this.getConec().getDbResultSet().getString("apellidos"));
               auxJugador.setNombre_usuario(this.getConec().getDbResultSet().getString("nombre_usuario"));
               auxJugador.setContrasena(this.getConec().getDbResultSet().getString("contrasena_usuario"));
               auxJugador.setEmail(this.getConec().getDbResultSet().getString("email"));
               auxJugador.setTelefono(this.getConec().getDbResultSet().getString("telefono"));
               //auxJugador.setFecha_nacimiento(this.getConec().getDbResultSet().getDate("fecha_nacimiento"));
               auxJugador.setId_posicion(this.getConec().getDbResultSet().getInt("id_posicion"));
               auxJugador.setId_comuna(this.getConec().getDbResultSet().getInt("codigo_comuna"));
               
               auxListadoJugadores.add(auxJugador);
               
           }
        
        }
        catch(Exception ex)
        {
           JOptionPane.showMessageDialog(null,"Error comando SQL " + ex.getMessage());
        }
        
        return auxListadoJugadores;
    }
    

    public Jugador buscarJugador(int id_jugador)
    {
        Jugador auxJugador = new Jugador();
        this.configurarConexion();
        this.getConec().setCadenaSQL("SELECT * FROM " + this.getConec().getNombreTabla()+
                                     " WHERE id_jugador = " +id_jugador +";" );
        this.getConec().setEsSelect(true);
        this.getConec().conectar();
        
        try
        {
           if(this.getConec().getDbResultSet().next())
           {
               auxJugador.setId_jugador(this.getConec().getDbResultSet().getInt("id_jugador"));
               auxJugador.setNombres(this.getConec().getDbResultSet().getString("nombres"));
               auxJugador.setApellidos(this.getConec().getDbResultSet().getString("apellidos"));
               auxJugador.setNombre_usuario(this.getConec().getDbResultSet().getString("nombre_usuario"));
               auxJugador.setContrasena(this.getConec().getDbResultSet().getString("contrasena_usuario"));
               auxJugador.setEmail(this.getConec().getDbResultSet().getString("email"));
               auxJugador.setTelefono(this.getConec().getDbResultSet().getString("telefono"));
               auxJugador.setFecha_nacimiento(this.getConec().getDbResultSet().getString("fecha_nacimiento"));
               auxJugador.setId_posicion(this.getConec().getDbResultSet().getInt("id_posicion"));
               auxJugador.setId_comuna(this.getConec().getDbResultSet().getInt("codigo_comuna"));
            
           }
           else{
                auxJugador.setId_jugador(0);
                auxJugador.setNombres("");
                auxJugador.setApellidos("");
                auxJugador.setNombre_usuario("");
                auxJugador.setContrasena("");
                auxJugador.setEmail("");
                auxJugador.setTelefono("");
                auxJugador.setFecha_nacimiento("");
                auxJugador.setId_posicion(0);
                auxJugador.setId_comuna(0);
           }
        
        }
        catch(Exception ex)
        {
            auxJugador.setId_jugador(0);
            auxJugador.setNombres("");
            auxJugador.setApellidos("");
            auxJugador.setNombre_usuario("");
            auxJugador.setContrasena("");
            auxJugador.setEmail("");
            auxJugador.setTelefono("");
            auxJugador.setFecha_nacimiento("");
            auxJugador.setId_posicion(0);
            auxJugador.setId_comuna(0);
        }
        
        return auxJugador;
    }
    
    public Jugador buscarJugadorNombreUsuario(String nombreUsuario)
    {
        Jugador auxJugador = new Jugador();
        this.configurarConexion();
        this.getConec().setCadenaSQL("SELECT * FROM " + this.getConec().getNombreTabla()+
                                     " WHERE nombre_usuario like '" +nombreUsuario +"';" );
        this.getConec().setEsSelect(true);
        this.getConec().conectar();
        
        try
        {
           if(this.getConec().getDbResultSet().next())
           {
               auxJugador.setId_jugador(this.getConec().getDbResultSet().getInt("id_jugador"));
               auxJugador.setNombres(this.getConec().getDbResultSet().getString("nombres"));
               auxJugador.setApellidos(this.getConec().getDbResultSet().getString("apellidos"));
               auxJugador.setNombre_usuario(this.getConec().getDbResultSet().getString("nombre_usuario"));
               auxJugador.setContrasena(this.getConec().getDbResultSet().getString("contrasena_usuario"));
               auxJugador.setEmail(this.getConec().getDbResultSet().getString("email"));
               auxJugador.setTelefono(this.getConec().getDbResultSet().getString("telefono"));
               auxJugador.setFecha_nacimiento(this.getConec().getDbResultSet().getString("fecha_nacimiento"));
               auxJugador.setId_posicion(this.getConec().getDbResultSet().getInt("id_posicion"));
               auxJugador.setId_comuna(this.getConec().getDbResultSet().getInt("codigo_comuna"));
            
           }
           else{
                auxJugador.setId_jugador(0);
                auxJugador.setNombres("");
                auxJugador.setApellidos("");
                auxJugador.setNombre_usuario("");
                auxJugador.setContrasena("");
                auxJugador.setEmail("");
                auxJugador.setTelefono("");
                auxJugador.setFecha_nacimiento("");
                auxJugador.setId_posicion(0);
                auxJugador.setId_comuna(0);
           }
        
        }
        catch(Exception ex)
        {
            auxJugador.setId_jugador(0);
            auxJugador.setNombres("");
            auxJugador.setApellidos("");
            auxJugador.setNombre_usuario("");
            auxJugador.setContrasena("");
            auxJugador.setEmail("");
            auxJugador.setTelefono("");
            auxJugador.setFecha_nacimiento("");
            auxJugador.setId_posicion(0);
            auxJugador.setId_comuna(0);
        }
        
        return auxJugador;
    }
    


    
    /**
     * @return the conec
     */
    public ConexionMySql getConec() {
        return conec;
    }

    /**
     * @param conec the conec to set
     */
    public void setConec(ConexionMySql conec) {
        this.conec = conec;
    }
}
