/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CapaNegocio;

import CapaConexion.ConexionMySql;
import CapaDTO.Cancha;
import CapaDTO.Disponibilidad;
import CapaDTO.Equipo;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Andres
 */
public class NegocioDisponibilidad {
    private ConexionMySql conec = new ConexionMySql();
    
    
    public void configurarConexion()
    {
       this.getConec().setNombreBaseDatos("jdbc:mysql://localhost/chutealapp");
       this.getConec().setNombreTabla("disponibilidad");
       this.getConec().setCadenaConexion("com.mysql.jdbc.Driver");
       this.getConec().setUsuario("root");
       this.getConec().setPass("");
    }
    
    public void insertarDisponibilidadEquipo(Disponibilidad disponibilidad)
    {
        this.configurarConexion();
        this.getConec().setCadenaSQL("INSERT INTO " + this.getConec().getNombreTabla()+
                                     " (id_disponibilidad,"
                                    + "id_equipo,"
                                    + "id_cancha,"
                                    + "id_dia,"
                                    + "hora_inicio,"
                                    + "hora_termino) VALUES "
                                    + "(null,"
                                    +disponibilidad.getId_equipo()+ ","
                                    + "0,"
                                    +disponibilidad.getId_dia()+ ","
                                    +"'"+disponibilidad.getHora_inicio()+ "',"
                                    +"'"+disponibilidad.getHora_termino()+ "');");
        this.getConec().setEsSelect(false);
        this.getConec().conectar();
    }
    
    public void insertarDisponibilidadCancha(Disponibilidad disponibilidad)
    {
        this.configurarConexion();
        this.getConec().setCadenaSQL("INSERT INTO " + this.getConec().getNombreTabla()+
                                     " (id_disponibilidad,"
                                    + "id_equipo,"
                                    + "id_cancha,"
                                    + "id_dia,"
                                    + "hora_inicio,"
                                    + "hora_termino) VALUES "
                                    + "(null,"
                                    + "0,"
                                    +disponibilidad.getId_cancha()+ ","
                                    +disponibilidad.getId_dia()+ ","
                                    +"'"+disponibilidad.getHora_inicio()+ "',"
                                    +"'"+disponibilidad.getHora_termino()+ "');");
        this.getConec().setEsSelect(false);
        this.getConec().conectar();
    }

    public ArrayList<Disponibilidad> obtenerDisponibilidadEquipo(Equipo equipo)
    {
        ArrayList<Disponibilidad> auxListaDisponibilidades = new ArrayList<>();
        this.configurarConexion();
        this.getConec().setCadenaSQL("SELECT * FROM " + this.getConec().getNombreTabla()
                + " JOIN equipo on disponibilidad.id_equipo = equipo.id_equipo "
                + " WHERE disponibilidad.id_equipo = "+equipo.getId_equipo()+";");
        this.getConec().setEsSelect(true);
        this.getConec().conectar();
        
        try
        {
            while(this.getConec().getDbResultSet().next())
           {
               Disponibilidad auxDisponibilidad = new Disponibilidad();
               auxDisponibilidad.setId_disponibilidad(this.getConec().getDbResultSet().getInt("id_disponibilidad"));
               auxDisponibilidad.setId_equipo(this.getConec().getDbResultSet().getInt("id_equipo"));
               auxDisponibilidad.setId_cancha(0);
               auxDisponibilidad.setId_dia(this.getConec().getDbResultSet().getInt("id_dia"));
               auxDisponibilidad.setHora_inicio(this.getConec().getDbResultSet().getString("hora_inicio"));
               auxDisponibilidad.setHora_termino(this.getConec().getDbResultSet().getString("hora_termino"));

               auxListaDisponibilidades.add(auxDisponibilidad);
               
           }
        }
        catch(Exception ex)
        {
           JOptionPane.showMessageDialog(null,"Error comando SQL " + ex.getMessage());
        }
        
        return auxListaDisponibilidades;
    }
    
    public ArrayList<Disponibilidad> obtenerDisponibilidadCancha(Cancha cancha)
    {
        ArrayList<Disponibilidad> auxListaDisponibilidades = new ArrayList<>();
        this.configurarConexion();
        this.getConec().setCadenaSQL("SELECT * FROM " + this.getConec().getNombreTabla()
                + " JOIN cancha on disponibilidad.id_cancha = cancha.id_cancha "
                + " WHERE disponibilidad.id_cancha = "+cancha.getId_cancha()+";");
        this.getConec().setEsSelect(true);
        this.getConec().conectar();
        
        try
        {
            while(this.getConec().getDbResultSet().next())
           {
               Disponibilidad auxDisponibilidad = new Disponibilidad();
               auxDisponibilidad.setId_disponibilidad(this.getConec().getDbResultSet().getInt("id_disponibilidad"));
               auxDisponibilidad.setId_equipo(0);
               auxDisponibilidad.setId_cancha(this.getConec().getDbResultSet().getInt("id_equipo"));
               auxDisponibilidad.setId_dia(this.getConec().getDbResultSet().getInt("id_dia"));
               auxDisponibilidad.setHora_inicio(this.getConec().getDbResultSet().getString("hora_inicio"));
               auxDisponibilidad.setHora_termino(this.getConec().getDbResultSet().getString("hora_termino"));

               auxListaDisponibilidades.add(auxDisponibilidad);
               
           }
        }
        catch(Exception ex)
        {
           JOptionPane.showMessageDialog(null,"Error comando SQL " + ex.getMessage());
        }
        
        return auxListaDisponibilidades;
    }

    /**
     * @return the conec
     */
    public ConexionMySql getConec() {
        return conec;
    }

    /**
     * @param conec the conec to set
     */
    public void setConec(ConexionMySql conec) {
        this.conec = conec;
    }
}
