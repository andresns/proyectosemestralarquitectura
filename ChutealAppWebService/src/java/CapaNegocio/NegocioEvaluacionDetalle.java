/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CapaNegocio;

import CapaConexion.ConexionMySql;
import CapaDTO.Cancha;
import CapaDTO.Equipo;
import CapaDTO.EvaluacionDetalle;
import CapaDTO.Jugador;
import CapaDTO.Valoracion;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Maltra
 */
public class NegocioEvaluacionDetalle {
    private ConexionMySql conec = new ConexionMySql();
    
    
    public void configurarConexion()
    {
       this.getConec().setNombreBaseDatos("jdbc:mysql://localhost/chutealapp");
       this.getConec().setNombreTabla("evaluacion_detalle");
       this.getConec().setCadenaConexion("com.mysql.jdbc.Driver");
       this.getConec().setUsuario("root");
       this.getConec().setPass("");
    }
    
    public void insertarComentarioJugador(EvaluacionDetalle evaluacionDetalle)
    {
        this.configurarConexion();
        this.getConec().setCadenaSQL("INSERT INTO " + this.getConec().getNombreTabla()+
                                     " (id_evaluacion_detalle,"
                                    + "id_cancha,"
                                    + "id_equipo,"
                                    + "id_evaluacion_cabecera,"
                                    + "id_jugador,"
                                    + "id_escala,"
                                    + "comentario) VALUES "
                                    + "(null,"
                                    +evaluacionDetalle.getId_eval_detalle()+ ","
                                    + "0,"
                                    + "0,"
                                    +evaluacionDetalle.getId_eval_cabecera()+ ","
                                    +evaluacionDetalle.getId_jugador()+ ","
                                    +evaluacionDetalle.getId_escala()+ ","
                                    +"'"+evaluacionDetalle.getComentario()+ "');");
        this.getConec().setEsSelect(false);
        this.getConec().conectar();
    }

    public void insertarComentarioCancha(EvaluacionDetalle evaluacionDetalle)
    {
        this.configurarConexion();
        this.getConec().setCadenaSQL("INSERT INTO " + this.getConec().getNombreTabla()+
                                     " (id_evaluacion_detalle,"
                                    + "id_cancha,"
                                    + "id_equipo,"
                                    + "id_evaluacion_cabecera,"
                                    + "id_jugador,"
                                    + "id_escala,"
                                    + "comentario) VALUES "
                                    + "(null,"
                                    +evaluacionDetalle.getId_eval_detalle()+ ","
                                    +evaluacionDetalle.getId_cancha()+ ","
                                    + "0,"
                                    +evaluacionDetalle.getId_eval_cabecera()+ ","
                                    + "0,"
                                    +evaluacionDetalle.getId_escala()+ ","
                                    +"'"+evaluacionDetalle.getComentario()+ "');");
        this.getConec().setEsSelect(false);
        this.getConec().conectar();
    }
    
    public void insertarComentarioEquipo(EvaluacionDetalle evaluacionDetalle)
    {
        this.configurarConexion();
        this.getConec().setCadenaSQL("INSERT INTO " + this.getConec().getNombreTabla()+
                                     " (id_evaluacion_detalle,"
                                    + "id_cancha,"
                                    + "id_equipo,"
                                    + "id_evaluacion_cabecera,"
                                    + "id_jugador,"
                                    + "id_escala,"
                                    + "comentario) VALUES "
                                    + "(null,"
                                    +evaluacionDetalle.getId_eval_detalle()+ ","
                                    + "0,"
                                    +evaluacionDetalle.getId_equipo()+ ","
                                    +evaluacionDetalle.getId_eval_cabecera()+ ","
                                    +"0,"
                                    +evaluacionDetalle.getId_escala()+ ","
                                    +"'"+evaluacionDetalle.getComentario()+ "');");
        this.getConec().setEsSelect(false);
        this.getConec().conectar();
    }
    
    public ArrayList<Valoracion> obtenerValoracionEquipo(Equipo equipo)
    {
        ArrayList<Valoracion> auxListaValoraciones = new ArrayList<>();
        this.configurarConexion();
        this.getConec().setCadenaSQL("SELECT * FROM " + this.getConec().getNombreTabla()
                + " JOIN escala on evaluacion_detalle.id_escala = escala.id_escala "
                + " WHERE evaluacion_detalle.id_equipo = "+equipo.getId_equipo()+";");
        this.getConec().setEsSelect(true);
        this.getConec().conectar();
        
        try
        {
            while(this.getConec().getDbResultSet().next())
           {
               Valoracion auxValoracion = new Valoracion();
               auxValoracion.setId_escala(this.getConec().getDbResultSet().getInt("id_escala"));
               auxValoracion.setValoracion(this.getConec().getDbResultSet().getDouble("valoracion"));

               auxListaValoraciones.add(auxValoracion);
               
           }
        }
        catch(Exception ex)
        {
           JOptionPane.showMessageDialog(null,"Error comando SQL " + ex.getMessage());
        }
        
        return auxListaValoraciones;
    }
    
    public ArrayList<EvaluacionDetalle> obtenerEvaluacionDetalleEquipo(Equipo equipo)
    {
        ArrayList<EvaluacionDetalle> auxListaEvaluacionDetalle = new ArrayList<>();
        this.configurarConexion();
        this.getConec().setCadenaSQL("SELECT * FROM " + this.getConec().getNombreTabla()
                + " JOIN evaluacion_cabecera on evaluacion_detalle.id_evaluacion_cabecera = evaluacion_cabecera.id_evaluacion_cabecera "
                + " WHERE evaluacion_detalle.id_equipo = "+equipo.getId_equipo()+";");
        this.getConec().setEsSelect(true);
        this.getConec().conectar();
        
        try
        {
            while(this.getConec().getDbResultSet().next())
           {
               EvaluacionDetalle auxEvaluacionDetalle = new EvaluacionDetalle();
               auxEvaluacionDetalle.setId_eval_detalle(this.getConec().getDbResultSet().getInt("evaluacion_detalle.id_evaluacion_detalle"));
               auxEvaluacionDetalle.setId_cancha(0);
               auxEvaluacionDetalle.setId_equipo(this.getConec().getDbResultSet().getInt("evaluacion_detalle.id_equipo"));
               auxEvaluacionDetalle.setId_eval_cabecera(this.getConec().getDbResultSet().getInt("evaluacion_cabecera.id_evaluacion_cabecera"));
               auxEvaluacionDetalle.setId_jugador(0);
               auxEvaluacionDetalle.setId_escala(this.getConec().getDbResultSet().getInt("evaluacion_detalle.id_escala"));
               auxEvaluacionDetalle.setComentario(this.getConec().getDbResultSet().getString("evaluacion_detalle.comentario"));

               auxListaEvaluacionDetalle.add(auxEvaluacionDetalle);
               
           }
        }
        catch(Exception ex)
        {
           JOptionPane.showMessageDialog(null,"Error comando SQL " + ex.getMessage());
        }
        
        return auxListaEvaluacionDetalle;
    }
    
    public ArrayList<EvaluacionDetalle> obtenerEvaluacionDetalleJugador(Jugador jugador)
    {
        ArrayList<EvaluacionDetalle> auxListaEvaluacionDetalle = new ArrayList<>();
        this.configurarConexion();
        this.getConec().setCadenaSQL("SELECT * FROM " + this.getConec().getNombreTabla()
                + " JOIN evaluacion_cabecera on evaluacion_detalle.id_evaluacion_cabecera = evaluacion_cabecera.id_evaluacion_cabecera "
                + " WHERE evaluacion_detalle.id_jugador = "+jugador.getId_jugador()+";");
        this.getConec().setEsSelect(true);
        this.getConec().conectar();
        
        try
        {
            while(this.getConec().getDbResultSet().next())
           {
               EvaluacionDetalle auxEvaluacionDetalle = new EvaluacionDetalle();
               auxEvaluacionDetalle.setId_eval_detalle(this.getConec().getDbResultSet().getInt("evaluacion_detalle.id_evaluacion_detalle"));
               auxEvaluacionDetalle.setId_cancha(0);
               auxEvaluacionDetalle.setId_equipo(0);
               auxEvaluacionDetalle.setId_eval_cabecera(this.getConec().getDbResultSet().getInt("evaluacion_cabecera.id_evaluacion_cabecera"));
               auxEvaluacionDetalle.setId_jugador(this.getConec().getDbResultSet().getInt("evaluacion_detalle.id_jugador"));
               auxEvaluacionDetalle.setId_escala(this.getConec().getDbResultSet().getInt("evaluacion_detalle.id_escala"));
               auxEvaluacionDetalle.setComentario(this.getConec().getDbResultSet().getString("evaluacion_detalle.comentario"));

               auxListaEvaluacionDetalle.add(auxEvaluacionDetalle);
               
           }
        }
        catch(Exception ex)
        {
           JOptionPane.showMessageDialog(null,"Error comando SQL " + ex.getMessage());
        }
        
        return auxListaEvaluacionDetalle;
    }
    
    public ArrayList<EvaluacionDetalle> obtenerEvaluacionDetalleCancha(Cancha cancha)
    {
        ArrayList<EvaluacionDetalle> auxListaEvaluacionDetalle = new ArrayList<>();
        this.configurarConexion();
        this.getConec().setCadenaSQL("SELECT * FROM " + this.getConec().getNombreTabla()
                + " JOIN evaluacion_cabecera on evaluacion_detalle.id_evaluacion_cabecera = evaluacion_cabecera.id_evaluacion_cabecera "
                + " WHERE evaluacion_detalle.id_cancha = "+cancha.getId_cancha()+";");
        this.getConec().setEsSelect(true);
        this.getConec().conectar();
        
        try
        {
            while(this.getConec().getDbResultSet().next())
           {
               EvaluacionDetalle auxEvaluacionDetalle = new EvaluacionDetalle();
               auxEvaluacionDetalle.setId_eval_detalle(this.getConec().getDbResultSet().getInt("evaluacion_detalle.id_evaluacion_detalle"));
               auxEvaluacionDetalle.setId_cancha(this.getConec().getDbResultSet().getInt("evaluacion_detalle.id_cancha"));
               auxEvaluacionDetalle.setId_equipo(0);
               auxEvaluacionDetalle.setId_eval_cabecera(this.getConec().getDbResultSet().getInt("evaluacion_cabecera.id_evaluacion_cabecera"));
               auxEvaluacionDetalle.setId_jugador(0);
               auxEvaluacionDetalle.setId_escala(this.getConec().getDbResultSet().getInt("evaluacion_detalle.id_escala"));
               auxEvaluacionDetalle.setComentario(this.getConec().getDbResultSet().getString("evaluacion_detalle.comentario"));

               auxListaEvaluacionDetalle.add(auxEvaluacionDetalle);
               
           }
        }
        catch(Exception ex)
        {
           JOptionPane.showMessageDialog(null,"Error comando SQL " + ex.getMessage());
        }
        
        return auxListaEvaluacionDetalle;
    }
    
    /**
     * @return the conec
     */
    public ConexionMySql getConec() {
        return conec;
    }

    /**
     * @param conec the conec to set
     */
    public void setConec(ConexionMySql conec) {
        this.conec = conec;
    }
}
