/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CapaNegocio;

import CapaConexion.ConexionMySql;
import CapaDTO.Comuna;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Andres
 */
public class NegocioComuna {
    private ConexionMySql conec = new ConexionMySql();
    
    public void configurarConexion()
    {
       this.getConec().setNombreBaseDatos("jdbc:mysql://localhost/chutealapp");
       this.getConec().setNombreTabla("comuna");
       this.getConec().setCadenaConexion("com.mysql.jdbc.Driver");
       this.getConec().setUsuario("root");
       this.getConec().setPass("");
    }

    
    //Listar todos los equipo
    public ArrayList<Comuna> listarComunas(int codigo_region)
    {
        ArrayList<Comuna> auxListadoRegiones = new ArrayList<>();
        this.configurarConexion();
        this.getConec().setCadenaSQL("SELECT * FROM " + this.getConec().getNombreTabla() + " "
                                    + "WHERE codigo_region = "+codigo_region+";");
        this.getConec().setEsSelect(true);
        this.getConec().conectar();
        
        try
        {
           while(this.getConec().getDbResultSet().next())
           {
               Comuna auxComuna = new Comuna();
               auxComuna.setCodigo_comuna(this.getConec().getDbResultSet().getInt("codigo_comuna"));
               auxComuna.setNombre(this.getConec().getDbResultSet().getString("nombre_comuna"));
               auxComuna.setCodigo_region(this.getConec().getDbResultSet().getInt("codigo_region"));

               auxListadoRegiones.add(auxComuna);
               
           }
        
        }
        catch(Exception ex)
        {
           JOptionPane.showMessageDialog(null,"Error comando SQL " + ex.getMessage());
        }
        
        return auxListadoRegiones;
    }
    
    public Comuna buscarComunaNombre(String nombreComuna)
    {
        Comuna auxComuna = new Comuna();
        this.configurarConexion();
        this.getConec().setCadenaSQL("SELECT * FROM " + this.getConec().getNombreTabla()+
                                     " WHERE upper(nombre_comuna) like upper('" +nombreComuna +"');" );
        this.getConec().setEsSelect(true);
        this.getConec().conectar();
        
        try
        {
           if(this.getConec().getDbResultSet().next())
           {
               auxComuna.setCodigo_comuna(this.getConec().getDbResultSet().getInt("codigo_comuna"));
               auxComuna.setNombre(this.getConec().getDbResultSet().getString("nombre_comuna"));
               auxComuna.setCodigo_region(this.getConec().getDbResultSet().getInt("codigo_region"));
           }
           else{
                auxComuna.setCodigo_comuna(0);
                auxComuna.setNombre("");
                auxComuna.setCodigo_region(0);

           }
        
        }
        catch(Exception ex)
        {
            auxComuna.setCodigo_comuna(0);
            auxComuna.setNombre("");
            auxComuna.setCodigo_region(0);

        }
        
        return auxComuna;
    }
    
    public Comuna buscarComunaId(int codigoComuna)
    {
        Comuna auxComuna = new Comuna();
        this.configurarConexion();
        this.getConec().setCadenaSQL("SELECT * FROM " + this.getConec().getNombreTabla()+
                                     " WHERE codigo_comuna =" +codigoComuna +";" );
        this.getConec().setEsSelect(true);
        this.getConec().conectar();
        
        try
        {
           if(this.getConec().getDbResultSet().next())
           {
               auxComuna.setCodigo_comuna(this.getConec().getDbResultSet().getInt("codigo_comuna"));
               auxComuna.setNombre(this.getConec().getDbResultSet().getString("nombre_comuna"));
               auxComuna.setCodigo_region(this.getConec().getDbResultSet().getInt("codigo_region"));
           }
           else{
                auxComuna.setCodigo_comuna(0);
                auxComuna.setNombre("");
                auxComuna.setCodigo_region(0);

           }
        
        }
        catch(Exception ex)
        {
            auxComuna.setCodigo_comuna(0);
            auxComuna.setNombre("");
            auxComuna.setCodigo_region(0);

        }
        
        return auxComuna;
    }

    
    /**
     * @return the conec
     */
    public ConexionMySql getConec() {
        return conec;
    }

    /**
     * @param conec the conec to set
     */
    public void setConec(ConexionMySql conec) {
        this.conec = conec;
    }
}
