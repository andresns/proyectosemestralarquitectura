/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CapaNegocio;

import CapaConexion.ConexionMySql;
import CapaDTO.Comuna;
import CapaDTO.Disponibilidad;
import CapaDTO.Equipo;
import CapaDTO.Jugador;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Andres
 */
public class NegocioEquipo {
    private ConexionMySql conec = new ConexionMySql();
    
    public void configurarConexion()
    {
       this.getConec().setNombreBaseDatos("jdbc:mysql://localhost/chutealapp");
       this.getConec().setNombreTabla("equipo");
       this.getConec().setCadenaConexion("com.mysql.jdbc.Driver");
       this.getConec().setUsuario("root");
       this.getConec().setPass("");
    }


    public void insertarEquipo(Equipo equipo)
    {
        this.configurarConexion();
        this.getConec().setCadenaSQL("INSERT INTO " + this.getConec().getNombreTabla()+
                                     " (id_equipo,"
                                    + "nombre_equipo,"
                                    + "nombre_contacto,"
                                    + "fono_contacto,"
                                    + "email_contacto,"
                                    + "codigo_comuna) VALUES "
                                    + "("+equipo.getId_equipo()+ ","
                                    + "'"+equipo.getNombre()+ "',"
                                    + "'"+equipo.getNombre_contacto()+ "',"
                                    + "'"+equipo.getFono_contacto()+ "',"
                                    + "'"+equipo.getEmail_contacto()+ "',"
                                    +equipo.getId_comuna()+ ");");
        this.getConec().setEsSelect(false);
        this.getConec().conectar();
    }
    
    public void actualizarEquipo(Equipo equipo)
    {
        this.configurarConexion();
        this.getConec().setCadenaSQL("UPDATE " + this.getConec().getNombreTabla()+
                                     " SET nombre_equipo = '"+equipo.getNombre()+"',"
                                        + "nombre_contacto = '"+equipo.getNombre_contacto()+"',"
                                        + "fono_contacto = '"+equipo.getFono_contacto()+"',"
                                        + "email_contacto = '"+equipo.getEmail_contacto()+"',"
                                        + "codigo_comuna = "+equipo.getId_comuna()
                                        + " WHERE id_equipo = "+equipo.getId_equipo()+";");
        this.getConec().setEsSelect(false);
        this.getConec().conectar();
    }

    public void eliminarEquipo(int id_equipo)
    {
        this.configurarConexion();
        this.getConec().setCadenaSQL("DELETE FROM " + this.getConec().getNombreTabla()+
                                     " WHERE id_equipo = "+  id_equipo + ";");
        this.getConec().setEsSelect(false);
        this.getConec().conectar();
    }
    
    //Listar todos los equipo
    public ArrayList<Equipo> listarEquipos()
    {
        ArrayList<Equipo> auxListadoEquipos = new ArrayList<>();
        this.configurarConexion();
        this.getConec().setCadenaSQL("SELECT * FROM " + this.getConec().getNombreTabla() + ";");
        this.getConec().setEsSelect(true);
        this.getConec().conectar();
        
        try
        {
           while(this.getConec().getDbResultSet().next())
           {
               Equipo auxEquipo = new Equipo();
               auxEquipo.setId_equipo(this.getConec().getDbResultSet().getInt("id_equipo"));
               auxEquipo.setNombre(this.getConec().getDbResultSet().getString("nombre_equipo"));
               auxEquipo.setNombre_contacto(this.getConec().getDbResultSet().getString("nombre_contacto"));
               auxEquipo.setFono_contacto(this.getConec().getDbResultSet().getString("fono_contacto"));
               auxEquipo.setEmail_contacto(this.getConec().getDbResultSet().getString("email_contacto"));
               auxEquipo.setId_comuna(this.getConec().getDbResultSet().getInt("codigo_comuna"));
               
               auxListadoEquipos.add(auxEquipo);
               
           }
        
        }
        catch(Exception ex)
        {
           JOptionPane.showMessageDialog(null,"Error comando SQL " + ex.getMessage());
        }
        
        return auxListadoEquipos;
    }
    
    public ArrayList<Equipo> listarEquiposDeJugador(Jugador jugador)
    {
        ArrayList<Equipo> auxListadoEquipos = new ArrayList<>();
        this.configurarConexion();
        this.getConec().setCadenaSQL("SELECT * FROM " + this.getConec().getNombreTabla() + ""
                + " JOIN jugador_equipo on equipo.id_equipo = jugador_equipo.id_equipo "
                + " WHERE id_jugador = "+jugador.getId_jugador()+";");
        this.getConec().setEsSelect(true);
        this.getConec().conectar();
        
        try
        {
           while(this.getConec().getDbResultSet().next())
           {
               Equipo auxEquipo = new Equipo();
               auxEquipo.setId_equipo(this.getConec().getDbResultSet().getInt("id_equipo"));
               auxEquipo.setNombre(this.getConec().getDbResultSet().getString("nombre_equipo"));
               auxEquipo.setNombre_contacto(this.getConec().getDbResultSet().getString("nombre_contacto"));
               auxEquipo.setFono_contacto(this.getConec().getDbResultSet().getString("fono_contacto"));
               auxEquipo.setEmail_contacto(this.getConec().getDbResultSet().getString("email_contacto"));
               auxEquipo.setId_comuna(this.getConec().getDbResultSet().getInt("codigo_comuna"));
               
               auxListadoEquipos.add(auxEquipo);
               
           }
        
        }
        catch(Exception ex)
        {
           JOptionPane.showMessageDialog(null,"Error comando SQL " + ex.getMessage());
        }
        
        return auxListadoEquipos;
    }
    
    public ArrayList<Equipo> listarEquiposRivales(Disponibilidad disponibilidad, Comuna comuna)
    {
        ArrayList<Equipo> auxListadoEquipos = new ArrayList<>();
        this.configurarConexion();
        this.getConec().setCadenaSQL("SELECT * FROM " + this.getConec().getNombreTabla() + ""
                + " JOIN disponibilidad on equipo.id_equipo = disponibilidad.id_equipo "
                + " WHERE equipo.codigo_comuna = "+comuna.getCodigo_comuna()
                + " AND disponibilidad.id_dia = "+disponibilidad.getId_dia()
                + " AND disponibilidad.hora_inicio >= '"+disponibilidad.getHora_inicio()+"'"
                + " AND disponibilidad.hora_termino <= '"+disponibilidad.getHora_termino()+"';");
        this.getConec().setEsSelect(true);
        this.getConec().conectar();
        
        try
        {
           while(this.getConec().getDbResultSet().next())
           {
               Equipo auxEquipo = new Equipo();
               auxEquipo.setId_equipo(this.getConec().getDbResultSet().getInt("equipo.id_equipo"));
               auxEquipo.setNombre(this.getConec().getDbResultSet().getString("equipo.nombre_equipo"));
               auxEquipo.setNombre_contacto(this.getConec().getDbResultSet().getString("equipo.nombre_contacto"));
               auxEquipo.setFono_contacto(this.getConec().getDbResultSet().getString("equipo.fono_contacto"));
               auxEquipo.setEmail_contacto(this.getConec().getDbResultSet().getString("equipo.email_contacto"));
               auxEquipo.setId_comuna(this.getConec().getDbResultSet().getInt("equipo.codigo_comuna"));
               
               auxListadoEquipos.add(auxEquipo);
               
           }
        
        }
        catch(Exception ex)
        {
           JOptionPane.showMessageDialog(null,"Error comando SQL " + ex.getMessage());
        }
        
        return auxListadoEquipos;
    }
    

    //Buscar Equipo por nombre
    public Equipo buscarEquipoNombre(String nombre_equipo)
    {
        Equipo auxEquipo = new Equipo();
        this.configurarConexion();
        this.getConec().setCadenaSQL("SELECT * FROM " + this.getConec().getNombreTabla()+
                                     " WHERE nombre_equipo = '" +nombre_equipo +"';" );
        this.getConec().setEsSelect(true);
        this.getConec().conectar();
        
        try
        {
           if(this.getConec().getDbResultSet().next())
           {
               auxEquipo.setId_equipo(this.getConec().getDbResultSet().getInt("id_equipo"));
               auxEquipo.setNombre(this.getConec().getDbResultSet().getString("nombre_equipo"));
               auxEquipo.setNombre_contacto(this.getConec().getDbResultSet().getString("nombre_contacto"));
               auxEquipo.setFono_contacto(this.getConec().getDbResultSet().getString("fono_contacto"));
               auxEquipo.setEmail_contacto(this.getConec().getDbResultSet().getString("email_contacto"));
               auxEquipo.setId_comuna(this.getConec().getDbResultSet().getInt("codigo_comuna"));
           }else{
                auxEquipo.setId_equipo(0);
                auxEquipo.setNombre("");
                auxEquipo.setNombre_contacto("");
                auxEquipo.setFono_contacto("");
                auxEquipo.setEmail_contacto("");
                auxEquipo.setId_comuna(0);

           }
        
        }
        catch(Exception ex)
        {
            auxEquipo.setId_equipo(0);
            auxEquipo.setNombre("");
            auxEquipo.setNombre_contacto("");
            auxEquipo.setFono_contacto("");
            auxEquipo.setEmail_contacto("");
            auxEquipo.setId_comuna(0);
        }
        
        return auxEquipo;
    }
    
    //Buscar equipos por comuna
    public ArrayList<Equipo> listarEquiposComuna(int codigo_comuna)
    {
        ArrayList<Equipo> auxListadoEquipos = new ArrayList<>();
        this.configurarConexion();
        this.getConec().setCadenaSQL("SELECT * FROM " + this.getConec().getNombreTabla() 
                                    + " WHERE codigo_comuna = "+codigo_comuna+";");
        this.getConec().setEsSelect(true);
        this.getConec().conectar();
        
        try
        {
           while(this.getConec().getDbResultSet().next())
           {
               Equipo auxEquipo = new Equipo();
               auxEquipo.setId_equipo(this.getConec().getDbResultSet().getInt("id_equipo"));
               auxEquipo.setNombre(this.getConec().getDbResultSet().getString("nombre_equipo"));
               auxEquipo.setNombre_contacto(this.getConec().getDbResultSet().getString("nombre_contacto"));
               auxEquipo.setFono_contacto(this.getConec().getDbResultSet().getString("fono_contacto"));
               auxEquipo.setEmail_contacto(this.getConec().getDbResultSet().getString("email_contacto"));
               auxEquipo.setId_comuna(this.getConec().getDbResultSet().getInt("codigo_comuna"));
               
               auxListadoEquipos.add(auxEquipo);
               
           }
        
        }
        catch(Exception ex)
        {
           JOptionPane.showMessageDialog(null,"Error comando SQL " + ex.getMessage());
        }
        
        return auxListadoEquipos;
    }
    
    public int idUltimoEquipo()
    {
        this.configurarConexion();
        this.getConec().setCadenaSQL("SELECT id_equipo FROM " + this.getConec().getNombreTabla()+
                                     " ORDER BY id_equipo DESC LIMIT 1;");
        this.getConec().setEsSelect(true);
        this.getConec().conectar();
        
        int auxIdEquipo = 0;
        try
        {
           if(this.getConec().getDbResultSet().next())
           {
               auxIdEquipo= this.getConec().getDbResultSet().getInt("id_equipo");

           }
        
        }
        catch(Exception ex)
        {
            auxIdEquipo = 0;
        }
        
        return auxIdEquipo;
    }
    
    
    
    /* EN ESTA HAY QUE HACER UN JOIN, ASI QUE QUEDA PENDIENTE POR AHORA */
    
    //Buscar equipos por comuna y disponibilidad
//    public ArrayList<Equipo> listarEquiposComunaHorario(int codigo_comuna, int id_disponibilidad)
//    {
//        ArrayList<Equipo> auxListadoEquipos = new ArrayList<>();
//        this.configurarConexion();
//        this.getConec().setCadenaSQL("SELECT * FROM " + this.getConec().getNombreTabla() + ""
//                                    + "WHERE codigo_comuna = "+codigo_comuna+";");
//        this.getConec().setEsSelect(true);
//        this.getConec().conectar();
//        
//        try
//        {
//           while(this.getConec().getDbResultSet().next())
//           {
//               Equipo auxEquipo = new Equipo();
//               auxEquipo.setId_equipo(this.getConec().getDbResultSet().getInt("id_equipo"));
//               auxEquipo.setNombre(this.getConec().getDbResultSet().getString("nombre_equipo"));
//               auxEquipo.setNombre_contacto(this.getConec().getDbResultSet().getString("nombre_contacto"));
//               auxEquipo.setFono_contacto(this.getConec().getDbResultSet().getString("fono_contacto"));
//               auxEquipo.setEmail_contacto(this.getConec().getDbResultSet().getString("email_contacto"));
//               auxEquipo.setId_comuna(this.getConec().getDbResultSet().getInt("codigo_comuna"));
//               
//               auxListadoEquipos.add(auxEquipo);
//               
//           }
//        
//        }
//        catch(Exception ex)
//        {
//           JOptionPane.showMessageDialog(null,"Error comando SQL " + ex.getMessage());
//        }
//        
//        return auxListadoEquipos;
//    }
    
    


    
    /**
     * @return the conec
     */
    public ConexionMySql getConec() {
        return conec;
    }

    /**
     * @param conec the conec to set
     */
    public void setConec(ConexionMySql conec) {
        this.conec = conec;
    }
}
