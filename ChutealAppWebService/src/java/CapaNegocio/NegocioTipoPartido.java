/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CapaNegocio;

import CapaConexion.ConexionMySql;
import CapaDTO.TipoPartido;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Maltra
 */
public class NegocioTipoPartido {
    
    private ConexionMySql conec = new ConexionMySql();
    
    public void configurarConexion()
    {
       this.getConec().setNombreBaseDatos("jdbc:mysql://localhost/chutealapp");
       this.getConec().setNombreTabla("tipo_partido");
       this.getConec().setCadenaConexion("com.mysql.jdbc.Driver");
       this.getConec().setUsuario("root");
       this.getConec().setPass("");
    }
    
    public void insertarTipoPartido(TipoPartido tipoPartido)
    {
        this.configurarConexion();
        this.getConec().setCadenaSQL("INSERT INTO " + this.getConec().getNombreTabla()+
                                     " (id_tipo_partido,"
                                    + "descripcion) VALUES "
                                    + "(null,"
                                    + "'"+tipoPartido.getDescripcion()+"');");
        this.getConec().setEsSelect(false);
        this.getConec().conectar();
    }
    
    public void actualizaTipoPartido(TipoPartido tipoPartido)
    {
        this.configurarConexion();
        this.getConec().setCadenaSQL("UPDATE " + this.getConec().getNombreTabla()+
                                     " SET descripcion = '"+tipoPartido.getDescripcion()+"'"
                                        + " WHERE id_tipo_partido = "+  tipoPartido.getId_tipo_partido()+ ";");
        this.getConec().setEsSelect(false);
        this.getConec().conectar();
    }

    public void eliminarTipoPartido(int id_tipo_partido)
    {
        this.configurarConexion();
        this.getConec().setCadenaSQL("DELETE FROM " + this.getConec().getNombreTabla()+
                                     " WHERE id_tipo_partido = "+  id_tipo_partido + ";");
        this.getConec().setEsSelect(false);
        this.getConec().conectar();
    }
    
    public ArrayList<TipoPartido> listaTipoPartidos()
    {
        ArrayList<TipoPartido> auxListaTipoPartidos = new ArrayList<>();
        this.configurarConexion();
        this.getConec().setCadenaSQL("SELECT * FROM " + this.getConec().getNombreTabla() + ";");
        this.getConec().setEsSelect(true);
        this.getConec().conectar();
        
        try
        {
           while(this.getConec().getDbResultSet().next())
           {
               TipoPartido auxTipoPartido = new TipoPartido();
               auxTipoPartido.setId_tipo_partido(this.getConec().getDbResultSet().getInt("id_tipo_partido"));
               auxTipoPartido.setDescripcion(this.getConec().getDbResultSet().getString("descripcion"));

               auxListaTipoPartidos.add(auxTipoPartido);
           }
        }
        catch(Exception ex)
        {
           JOptionPane.showMessageDialog(null,"Error comando SQL " + ex.getMessage());
        }
        return auxListaTipoPartidos;
    }
    

    public TipoPartido buscarTipoPartido(String descripcion)
    {
        TipoPartido auxTipoPartido = new TipoPartido();
        this.configurarConexion();
        this.getConec().setCadenaSQL("SELECT * FROM " + this.getConec().getNombreTabla()+
                                     " WHERE descripcion ='" +descripcion +"';" );
        this.getConec().setEsSelect(true);
        this.getConec().conectar();
        
        try
        {
           if(this.getConec().getDbResultSet().next())
           {
               auxTipoPartido.setId_tipo_partido(this.getConec().getDbResultSet().getInt("id_tipo_partido"));
               auxTipoPartido.setDescripcion(this.getConec().getDbResultSet().getString("descripcion"));
           }
           else{
               auxTipoPartido.setId_tipo_partido(0);
               auxTipoPartido.setDescripcion("");
           }
        
        }
        catch(Exception ex)
        {
            auxTipoPartido.setId_tipo_partido(0);
            auxTipoPartido.setDescripcion("");
        }
        
        return auxTipoPartido;
    }
    
    

    /**
     * @return the conec
     */
    public ConexionMySql getConec() {
        return conec;
    }

    /**
     * @param conec the conec to set
     */
    public void setConec(ConexionMySql conec) {
        this.conec = conec;
    }
    
}
