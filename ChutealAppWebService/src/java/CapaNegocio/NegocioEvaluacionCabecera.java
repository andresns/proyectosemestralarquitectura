/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CapaNegocio;

import CapaConexion.ConexionMySql;
import CapaDTO.EvaluacionCabecera;

/**
 *
 * @author Andres
 */
public class NegocioEvaluacionCabecera {
    private ConexionMySql conec = new ConexionMySql();
    
    
    public void configurarConexion()
    {
       this.getConec().setNombreBaseDatos("jdbc:mysql://localhost/chutealapp");
       this.getConec().setNombreTabla("evaluacion_cabecera");
       this.getConec().setCadenaConexion("com.mysql.jdbc.Driver");
       this.getConec().setUsuario("root");
       this.getConec().setPass("");
    }
    
    
    public void insertarEvaluacionCabecera(EvaluacionCabecera evaluacionCabecera)
    {
        this.configurarConexion();
        this.getConec().setCadenaSQL("INSERT INTO " + this.getConec().getNombreTabla()+
                                     " (id_evaluacion_cabecera,"
                                    + "id_jugador) VALUES "
                                    + "(null,"
                                    +""+evaluacionCabecera.getId_jugador()+ ");");
        this.getConec().setEsSelect(false);
        this.getConec().conectar();
    }
    
    public EvaluacionCabecera obtenerEvaluacionCabecera(int id_evaluacion_cabecera)
    {
        EvaluacionCabecera auxEvaluacionCabecera = new EvaluacionCabecera();
        this.configurarConexion();
        this.getConec().setCadenaSQL("SELECT * FROM " + this.getConec().getNombreTabla()
                + " WHERE id_evaluacion_cabecera = "+id_evaluacion_cabecera+";");
        this.getConec().setEsSelect(true);
        this.getConec().conectar();
        
        try
        {
           if(this.getConec().getDbResultSet().next())
           {
               auxEvaluacionCabecera.setId_evaluacion_cabecera(this.getConec().getDbResultSet().getInt("id_evaluacion_cabecera"));
               auxEvaluacionCabecera.setId_jugador(this.getConec().getDbResultSet().getInt("id_jugador"));

           }else{
                auxEvaluacionCabecera.setId_evaluacion_cabecera(0);
                auxEvaluacionCabecera.setId_jugador(0);


           }
        
        }
        catch(Exception ex)
        {
            auxEvaluacionCabecera.setId_evaluacion_cabecera(0);
                auxEvaluacionCabecera.setId_jugador(0);
        }
        
        return auxEvaluacionCabecera;
    }

    /**
     * @return the conec
     */
    public ConexionMySql getConec() {
        return conec;
    }

    /**
     * @param conec the conec to set
     */
    public void setConec(ConexionMySql conec) {
        this.conec = conec;
    }
}
