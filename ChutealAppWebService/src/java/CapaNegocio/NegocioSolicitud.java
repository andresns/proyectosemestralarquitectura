/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CapaNegocio;

import CapaConexion.ConexionMySql;
import CapaDTO.Solicitud;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Andres
 */
public class NegocioSolicitud {
    
    private ConexionMySql conec = new ConexionMySql();
    
    public void configurarConexion()
    {
       this.getConec().setNombreBaseDatos("jdbc:mysql://localhost/chutealapp");
       this.getConec().setNombreTabla("solicitud");
       this.getConec().setCadenaConexion("com.mysql.jdbc.Driver");
       this.getConec().setUsuario("root");
       this.getConec().setPass("");
    }
    
    public void insertarSolicitud(Solicitud solicitud)
    {
        this.configurarConexion();
        this.getConec().setCadenaSQL("INSERT INTO " + this.getConec().getNombreTabla()+
                                     " (id_solicitud,"
                                    + "id_jugador,"
                                    + "jug_id_jugador,"
                                    + "id_equipo,"
                                    + "equ_id_equipo,"
                                    + "estado_solicitud) VALUES "
                                    + "(null,"
                                    +solicitud.getId_jugador_1()+ ","
                                    +solicitud.getId_jugador_2()+ ","
                                    +solicitud.getId_equipo_1()+ ","
                                    +solicitud.getId_equipo_2()+ ","
                                    +solicitud.getEstado()+ ");");
        this.getConec().setEsSelect(false);
        this.getConec().conectar();
    }
    
    
    public ArrayList<Solicitud> listarSolicitudes()
    {
        ArrayList<Solicitud> auxListaSolicitudes = new ArrayList<>();
        this.configurarConexion();
        this.getConec().setCadenaSQL("SELECT * FROM " + this.getConec().getNombreTabla() + ";");
        this.getConec().setEsSelect(true);
        this.getConec().conectar();
        
        try
        {
           while(this.getConec().getDbResultSet().next())
           {
               Solicitud auxSolicitud = new Solicitud();
               auxSolicitud.setId_solicitud(this.getConec().getDbResultSet().getInt("id_solicitud"));
               auxSolicitud.setId_jugador_1(this.getConec().getDbResultSet().getInt("id_jugador"));
               auxSolicitud.setId_jugador_2(this.getConec().getDbResultSet().getInt("jug_id_jugador"));
               auxSolicitud.setId_equipo_1(this.getConec().getDbResultSet().getInt("id_equipo"));
               auxSolicitud.setId_equipo_2(this.getConec().getDbResultSet().getInt("equ_id_equipo"));
               auxSolicitud.setEstado(this.getConec().getDbResultSet().getInt("estado_solicitud"));
               
               auxListaSolicitudes.add(auxSolicitud);
               
           }
        
        }
        catch(Exception ex)
        {
           JOptionPane.showMessageDialog(null,"Error comando SQL " + ex.getMessage());
        }
        
        return auxListaSolicitudes;
    }
    
    public Solicitud buscarSolicitudPorId(int idSolicitud)
    {
        Solicitud auxSolicitud = new Solicitud();
        this.configurarConexion();
        this.getConec().setCadenaSQL("SELECT * FROM " + this.getConec().getNombreTabla()+
                                     " WHERE id_solicitud = " +idSolicitud +";" );
        this.getConec().setEsSelect(true);
        this.getConec().conectar();
        
        try
        {
           if(this.getConec().getDbResultSet().next())
           {
               auxSolicitud.setId_solicitud(this.getConec().getDbResultSet().getInt("id_solicitud"));
               auxSolicitud.setId_jugador_1(this.getConec().getDbResultSet().getInt("id_jugador"));
               auxSolicitud.setId_jugador_2(this.getConec().getDbResultSet().getInt("jug_id_jugador"));
               auxSolicitud.setId_equipo_1(this.getConec().getDbResultSet().getInt("id_equipo"));
               auxSolicitud.setId_equipo_2(this.getConec().getDbResultSet().getInt("equ_id_equipo"));
               auxSolicitud.setEstado(this.getConec().getDbResultSet().getInt("estado_solicitud"));
           }else{
                auxSolicitud.setId_solicitud(0);
                auxSolicitud.setId_jugador_1(0);
                auxSolicitud.setId_jugador_2(0);
                auxSolicitud.setId_equipo_1(0);
                auxSolicitud.setId_equipo_2(0);
                auxSolicitud.setEstado(0);

           }
        
        }
        catch(Exception ex)
        {
            auxSolicitud.setId_solicitud(0);
            auxSolicitud.setId_jugador_1(0);
            auxSolicitud.setId_jugador_2(0);
            auxSolicitud.setId_equipo_1(0);
            auxSolicitud.setId_equipo_2(0);
            auxSolicitud.setEstado(0);
        }
        
        return auxSolicitud;
    }

    
    
    /**
     * @return the conec
     */
    public ConexionMySql getConec() {
        return conec;
    }

    /**
     * @param conec the conec to set
     */
    public void setConec(ConexionMySql conec) {
        this.conec = conec;
    }
    
}
