/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CapaNegocio;

import CapaConexion.ConexionMySql;
import CapaDTO.Dia;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Andres
 */
public class NegocioDia {
    private ConexionMySql conec = new ConexionMySql();
    
    public void configurarConexion()
    {
       this.getConec().setNombreBaseDatos("jdbc:mysql://localhost/chutealapp");
       this.getConec().setNombreTabla("dia");
       this.getConec().setCadenaConexion("com.mysql.jdbc.Driver");
       this.getConec().setUsuario("root");
       this.getConec().setPass("");
    }


    public void insertarDia(Dia dia)
    {
        this.configurarConexion();
        this.getConec().setCadenaSQL("INSERT INTO " + this.getConec().getNombreTabla()+
                                     " (id_dia,"
                                    + "nombre_dia) VALUES "
                                    + "(null,"
                                    +"'"+dia.getNombre_dia()+ "');");
        this.getConec().setEsSelect(false);
        this.getConec().conectar();
    }
    
    public void modificarDia(Dia dia)
    {
        this.configurarConexion();
        this.getConec().setCadenaSQL("UPDATE " + this.getConec().getNombreTabla()+
                                     " SET nombre_dia = '"+dia.getNombre_dia()+"'"
                                        + " WHERE id_dia = "+  dia.getId_dia()+ ";");
        this.getConec().setEsSelect(false);
        this.getConec().conectar();
    }

    public void eliminarDia(int id_dia)
    {
        this.configurarConexion();
        this.getConec().setCadenaSQL("DELETE FROM " + this.getConec().getNombreTabla()+
                                     " WHERE id_dia = "+  id_dia + ";");
        this.getConec().setEsSelect(false);
        this.getConec().conectar();
    }
    
    //Listar todos los dias
    public ArrayList<Dia> listarDias()
    {
        ArrayList<Dia> auxListadoDias = new ArrayList<>();
        this.configurarConexion();
        this.getConec().setCadenaSQL("SELECT * FROM " + this.getConec().getNombreTabla() + ";");
        this.getConec().setEsSelect(true);
        this.getConec().conectar();
        
        try
        {
           while(this.getConec().getDbResultSet().next())
           {
               Dia auxDia = new Dia();
               auxDia.setId_dia(this.getConec().getDbResultSet().getInt("id_dia"));
               auxDia.setNombre_dia(this.getConec().getDbResultSet().getString("nombre_dia"));

               auxListadoDias.add(auxDia);
           }
        }
        catch(Exception ex)
        {
           JOptionPane.showMessageDialog(null,"Error comando SQL " + ex.getMessage());
        }
        
        return auxListadoDias;
    }
    

    public Dia buscarDiaId(int id_dia)
    {
        Dia auxDia = new Dia();
        this.configurarConexion();
        this.getConec().setCadenaSQL("SELECT * FROM " + this.getConec().getNombreTabla()+
                                     " WHERE id_dia = " +id_dia +";" );
        this.getConec().setEsSelect(true);
        this.getConec().conectar();
        
        try
        {
           if(this.getConec().getDbResultSet().next())
           {
               auxDia.setId_dia(this.getConec().getDbResultSet().getInt("id_dia"));
               auxDia.setNombre_dia(this.getConec().getDbResultSet().getString("nombre_dia"));

           }else{
                auxDia.setId_dia(0);
                auxDia.setNombre_dia("");
           }
        
        }
        catch(Exception ex)
        {
            auxDia.setId_dia(0);
            auxDia.setNombre_dia("");
        }
        
        return auxDia;
    }
    
    public Dia buscarDiaNombre(String nombre_dia)
    {
        Dia auxDia = new Dia();
        this.configurarConexion();
        this.getConec().setCadenaSQL("SELECT * FROM " + this.getConec().getNombreTabla()+
                                     " WHERE nombre_dia = '" +nombre_dia +"';" );
        this.getConec().setEsSelect(true);
        this.getConec().conectar();
        
        try
        {
           if(this.getConec().getDbResultSet().next())
           {
               auxDia.setId_dia(this.getConec().getDbResultSet().getInt("id_dia"));
               auxDia.setNombre_dia(this.getConec().getDbResultSet().getString("nombre_dia"));

           }else{
                auxDia.setId_dia(0);
                auxDia.setNombre_dia("");
           }
        
        }
        catch(Exception ex)
        {
            auxDia.setId_dia(0);
            auxDia.setNombre_dia("");
        }
        
        return auxDia;
    }

    /**
     * @return the conec
     */
    public ConexionMySql getConec() {
        return conec;
    }

    /**
     * @param conec the conec to set
     */
    public void setConec(ConexionMySql conec) {
        this.conec = conec;
    }
}
