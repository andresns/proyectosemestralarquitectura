/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CapaNegocio;

import CapaConexion.ConexionMySql;
import CapaDTO.Posicion;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Andres
 */
public class NegocioPosicion {
    private ConexionMySql conec = new ConexionMySql();
    
    public void configurarConexion()
    {
       this.getConec().setNombreBaseDatos("jdbc:mysql://localhost/chutealapp");
       this.getConec().setNombreTabla("posicion");
       this.getConec().setCadenaConexion("com.mysql.jdbc.Driver");
       this.getConec().setUsuario("root");
       this.getConec().setPass("");
    }
    
    public void insertarPosicion(Posicion posicion)
    {
        this.configurarConexion();
        this.getConec().setCadenaSQL("INSERT INTO " + this.getConec().getNombreTabla()+
                                    " (id_posicion,"
                                    + "nombre_posicion) VALUES "
                                    + "(null,"
                                    + "'"+posicion.getNombre_posicion()+"');");
        this.getConec().setEsSelect(false);
        this.getConec().conectar();
    }
    
    public void actualizaPosicion(Posicion posicion)
    {
        this.configurarConexion();
        this.getConec().setCadenaSQL("UPDATE " + this.getConec().getNombreTabla()+
                                     " SET nombre_posicion = '"+posicion.getNombre_posicion()+"'"
                                     + " WHERE id_posicion = "+  posicion.getId_posicion()+ ";");
        this.getConec().setEsSelect(false);
        this.getConec().conectar();
    }
    
    public void eliminaPosicion(int id_posicion)
    {
        this.configurarConexion();
        this.getConec().setCadenaSQL("DELETE FROM " + this.getConec().getNombreTabla()+
                                     " WHERE id_posicion = "+  id_posicion + ";");
        this.getConec().setEsSelect(false);
        this.getConec().conectar();
    }

    public ArrayList<Posicion> listarPosiciones()
    {
        ArrayList<Posicion> auxListadoPosiciones = new ArrayList<>();
        this.configurarConexion();
        this.getConec().setCadenaSQL("SELECT * FROM " + this.getConec().getNombreTabla() +";");
        this.getConec().setEsSelect(true);
        this.getConec().conectar();
        
        try
        {
           while(this.getConec().getDbResultSet().next())
           {
               Posicion auxPosicion = new Posicion();
               auxPosicion.setId_posicion(this.getConec().getDbResultSet().getInt("id_posicion"));
               auxPosicion.setNombre_posicion(this.getConec().getDbResultSet().getString("nombre_posicion"));
               
               auxListadoPosiciones.add(auxPosicion);
               
           }
        
        }
        catch(Exception ex)
        {
           JOptionPane.showMessageDialog(null,"Error comando SQL " + ex.getMessage());
        }
        
        return auxListadoPosiciones;
    }

    public Posicion buscarPosicionId(int id_posicion)
    {
        Posicion auxPosicion = new Posicion();
        this.configurarConexion();
        this.getConec().setCadenaSQL("SELECT * FROM " + this.getConec().getNombreTabla()+
                                     " WHERE id_posicion =" +id_posicion +";" );
        this.getConec().setEsSelect(true);
        this.getConec().conectar();
        
        try
        {
           if(this.getConec().getDbResultSet().next())
           {
               auxPosicion.setId_posicion(this.getConec().getDbResultSet().getInt("id_posicion"));
               auxPosicion.setNombre_posicion(this.getConec().getDbResultSet().getString("nombre_posicion"));
           }
           else{
                auxPosicion.setId_posicion(0);
                auxPosicion.setNombre_posicion("");
           }
        
        }
        catch(Exception ex)
        {
            auxPosicion.setId_posicion(0);
            auxPosicion.setNombre_posicion("");
        }
        
        return auxPosicion;
    }
    
    public Posicion buscarPosicionNombre(String nombre_posicion)
    {
        Posicion auxPosicion = new Posicion();
        this.configurarConexion();
        this.getConec().setCadenaSQL("SELECT * FROM " + this.getConec().getNombreTabla()+
                                     " WHERE nombre_posicion ='" +nombre_posicion +"';" );
        this.getConec().setEsSelect(true);
        this.getConec().conectar();
        
        try
        {
           if(this.getConec().getDbResultSet().next())
           {
               auxPosicion.setId_posicion(this.getConec().getDbResultSet().getInt("id_posicion"));
               auxPosicion.setNombre_posicion(this.getConec().getDbResultSet().getString("nombre_posicion"));
           }
           else{
                auxPosicion.setId_posicion(0);
                auxPosicion.setNombre_posicion("");
           }
        
        }
        catch(Exception ex)
        {
            auxPosicion.setId_posicion(0);
            auxPosicion.setNombre_posicion("");
        }
        
        return auxPosicion;
    }
    
    /**
     * @return the conec
     */
    public ConexionMySql getConec() {
        return conec;
    }

    /**
     * @param conec the conec to set
     */
    public void setConec(ConexionMySql conec) {
        this.conec = conec;
    }
}
