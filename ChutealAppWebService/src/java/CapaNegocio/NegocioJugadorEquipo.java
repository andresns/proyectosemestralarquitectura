/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CapaNegocio;

import CapaConexion.ConexionMySql;
import CapaDTO.Equipo;
import CapaDTO.Jugador;
import CapaDTO.JugadorEquipo;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Andres
 */
public class NegocioJugadorEquipo {
    private ConexionMySql conec = new ConexionMySql();
    
    public void configurarConexion()
    {
       this.getConec().setNombreBaseDatos("jdbc:mysql://localhost/chutealapp");
       this.getConec().setNombreTabla("jugador_equipo");
       this.getConec().setCadenaConexion("com.mysql.jdbc.Driver");
       this.getConec().setUsuario("root");
       this.getConec().setPass("");
    }


    public void insertarJugadorEquipo(int id_jugador, int id_equipo)
    {
        this.configurarConexion();
        this.getConec().setCadenaSQL("INSERT INTO " + this.getConec().getNombreTabla()+
                                     " (id_jugador,"
                                    + "id_equipo) VALUES "
                                    + "("+id_jugador+ ","
                                    +id_equipo+ ");");
        this.getConec().setEsSelect(false);
        this.getConec().conectar();
    }
    
    public Equipo buscarJugadorEnEquipo(Jugador jugador)
    {
        this.configurarConexion();
        this.getConec().setCadenaSQL("SELECT * FROM " + this.getConec().getNombreTabla()+
                                     " JOIN equipo on jugador_equipo.id_equipo = equipo.id_equipo "
                                    + "WHERE id_jugador = " +jugador.getId_jugador() +";" );
        this.getConec().setEsSelect(true);
        this.getConec().conectar();

        Equipo auxEquipo = new Equipo();
        try
        {
           if(this.getConec().getDbResultSet().next())
           {
               auxEquipo.setId_equipo(this.getConec().getDbResultSet().getInt("equipo.id_equipo"));
               auxEquipo.setNombre(this.getConec().getDbResultSet().getString("equipo.nombre_equipo"));
               auxEquipo.setNombre_contacto(this.getConec().getDbResultSet().getString("equipo.nombre_contacto"));
               auxEquipo.setFono_contacto(this.getConec().getDbResultSet().getString("equipo.fono_contacto"));
               auxEquipo.setEmail_contacto(this.getConec().getDbResultSet().getString("equipo.email_contacto"));
               auxEquipo.setId_comuna(this.getConec().getDbResultSet().getInt("equipo.codigo_comuna"));
               
           }
           else{
                auxEquipo.setId_equipo(0);
                auxEquipo.setNombre("");
                auxEquipo.setNombre_contacto("");
                auxEquipo.setFono_contacto("");
                auxEquipo.setEmail_contacto("");
                auxEquipo.setId_comuna(0);
           }
        
        }
        catch(Exception ex)
        {
            auxEquipo.setId_equipo(0);
            auxEquipo.setNombre("");
            auxEquipo.setNombre_contacto("");
            auxEquipo.setFono_contacto("");
            auxEquipo.setEmail_contacto("");
            auxEquipo.setId_comuna(0);
        }
        
        return auxEquipo;
    }
    
    public ArrayList<Jugador> listarJugadoresDeEquipo(Equipo equipo)
    {
        ArrayList<Jugador> auxListadoJugadoresDeEquipo = new ArrayList<>();
        this.configurarConexion();
        this.getConec().setCadenaSQL("SELECT * FROM " + this.getConec().getNombreTabla() 
                                    + " JOIN jugador on jugador_equipo.id_jugador = jugador.id_jugador"
                                    + " WHERE jugador_equipo.id_equipo = "+equipo.getId_equipo()+";");
        this.getConec().setEsSelect(true);
        this.getConec().conectar();
        
        try
        {
           while(this.getConec().getDbResultSet().next())
           {
               Jugador auxJugador = new Jugador();
               auxJugador.setId_jugador(this.getConec().getDbResultSet().getInt("id_jugador"));
               auxJugador.setNombres(this.getConec().getDbResultSet().getString("nombres"));
               auxJugador.setApellidos(this.getConec().getDbResultSet().getString("apellidos"));
               auxJugador.setNombre_usuario(this.getConec().getDbResultSet().getString("nombre_usuario"));
               auxJugador.setContrasena(this.getConec().getDbResultSet().getString("contrasena_usuario"));
               auxJugador.setEmail(this.getConec().getDbResultSet().getString("email"));
               auxJugador.setTelefono(this.getConec().getDbResultSet().getString("telefono"));
               //auxJugador.setFecha_nacimiento(this.getConec().getDbResultSet().getDate("fecha_nacimiento"));
               auxJugador.setId_posicion(this.getConec().getDbResultSet().getInt("id_posicion"));
               auxJugador.setId_comuna(this.getConec().getDbResultSet().getInt("codigo_comuna"));
               
               auxListadoJugadoresDeEquipo.add(auxJugador);
               
           }
        
        }
        catch(Exception ex)
        {
           JOptionPane.showMessageDialog(null,"Error comando SQL " + ex.getMessage());
        }
        
        return auxListadoJugadoresDeEquipo;
    }
    
    public void eliminarJugadorDeEquipo(int id_jugador, int id_equipo)
    {
        this.configurarConexion();
        this.getConec().setCadenaSQL("DELETE FROM " + this.getConec().getNombreTabla()
                                    + " WHERE id_jugador = "+id_jugador
                                    + " and id_equipo = "+id_equipo+";");
        this.getConec().setEsSelect(false);
        this.getConec().conectar();
    }
    
        /**
     * @return the conec
     */
    public ConexionMySql getConec() {
        return conec;
    }

    /**
     * @param conec the conec to set
     */
    public void setConec(ConexionMySql conec) {
        this.conec = conec;
    }
}
