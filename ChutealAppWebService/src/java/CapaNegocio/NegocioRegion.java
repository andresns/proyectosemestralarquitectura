/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CapaNegocio;

import CapaConexion.ConexionMySql;
import CapaDTO.Region;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Andres
 */
public class NegocioRegion {
    private ConexionMySql conec = new ConexionMySql();
    
    public void configurarConexion()
    {
       this.getConec().setNombreBaseDatos("jdbc:mysql://localhost/chutealapp");
       this.getConec().setNombreTabla("region");
       this.getConec().setCadenaConexion("com.mysql.jdbc.Driver");
       this.getConec().setUsuario("root");
       this.getConec().setPass("");
    }

    
    //Listar todos los equipo
    public ArrayList<Region> listarRegiones()
    {
        ArrayList<Region> auxListadoRegiones = new ArrayList<>();
        this.configurarConexion();
        this.getConec().setCadenaSQL("SELECT * FROM " + this.getConec().getNombreTabla() + ";");
        this.getConec().setEsSelect(true);
        this.getConec().conectar();
        
        try
        {
           while(this.getConec().getDbResultSet().next())
           {
               Region auxRegion = new Region();
               auxRegion.setCodigo_region(this.getConec().getDbResultSet().getInt("codigo_region"));
               auxRegion.setNombre(this.getConec().getDbResultSet().getString("nombre_region"));

               auxListadoRegiones.add(auxRegion);
               
           }
        
        }
        catch(Exception ex)
        {
           JOptionPane.showMessageDialog(null,"Error comando SQL " + ex.getMessage());
        }
        
        return auxListadoRegiones;
    }
    
    public Region buscarRegionNombre(String nombreRegion)
    {
        Region auxRegion = new Region();
        this.configurarConexion();
        this.getConec().setCadenaSQL("SELECT * FROM " + this.getConec().getNombreTabla()+
                                     " WHERE upper(nombre_region) like upper('" +nombreRegion +"');" );
        this.getConec().setEsSelect(true);
        this.getConec().conectar();
        
        try
        {
           if(this.getConec().getDbResultSet().next())
           {
               auxRegion.setCodigo_region(this.getConec().getDbResultSet().getInt("codigo_region"));
               auxRegion.setNombre(this.getConec().getDbResultSet().getString("nombre_region"));
           }
           else{
                auxRegion.setCodigo_region(0);
                auxRegion.setNombre("");

           }
        
        }
        catch(Exception ex)
        {
            auxRegion.setCodigo_region(0);
            auxRegion.setNombre("");

        }
        
        return auxRegion;
    }
    
    public Region buscarRegionId(int codigoRegion)
    {
        Region auxRegion = new Region();
        this.configurarConexion();
        this.getConec().setCadenaSQL("SELECT * FROM " + this.getConec().getNombreTabla()+
                                     " WHERE codigo_region =" +codigoRegion +";" );
        this.getConec().setEsSelect(true);
        this.getConec().conectar();
        
        try
        {
           if(this.getConec().getDbResultSet().next())
           {
               auxRegion.setCodigo_region(this.getConec().getDbResultSet().getInt("codigo_region"));
               auxRegion.setNombre(this.getConec().getDbResultSet().getString("nombre_region"));
           }
           else{
                auxRegion.setCodigo_region(0);
                auxRegion.setNombre("");

           }
        
        }
        catch(Exception ex)
        {
            auxRegion.setCodigo_region(0);
            auxRegion.setNombre("");

        }
        
        return auxRegion;
    }

    
    /**
     * @return the conec
     */
    public ConexionMySql getConec() {
        return conec;
    }

    /**
     * @param conec the conec to set
     */
    public void setConec(ConexionMySql conec) {
        this.conec = conec;
    }
}
