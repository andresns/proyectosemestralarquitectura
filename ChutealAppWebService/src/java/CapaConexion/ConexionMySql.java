/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CapaConexion;

import java.sql.*;
import javax.swing.JOptionPane;

/**
 *
 * @author Andres
 */
public class ConexionMySql {
    private String nombreBaseDatos;
    private String nombreTabla;
    private String cadenaConexion;
    private String cadenaSQL;
    private boolean esSelect;
    private Connection dbConnection;
    private ResultSet dbResultSet;
    private String usuario;
    private String pass;

    /**
     * @return the nombreBaseDatos
     */
    public String getNombreBaseDatos() {
        return nombreBaseDatos;
    }

    /**
     * @param nombreBaseDatos the nombreBaseDatos to set
     */
    public void setNombreBaseDatos(String nombreBaseDatos) {
        this.nombreBaseDatos = nombreBaseDatos;
    }

    /**
     * @return the nombreTabla
     */
    public String getNombreTabla() {
        return nombreTabla;
    }

    /**
     * @param nombreTabla the nombreTabla to set
     */
    public void setNombreTabla(String nombreTabla) {
        this.nombreTabla = nombreTabla;
    }

    /**
     * @return the cadenaConexion
     */
    public String getCadenaConexion() {
        return cadenaConexion;
    }

    /**
     * @param cadenaConexion the cadenaConexion to set
     */
    public void setCadenaConexion(String cadenaConexion) {
        this.cadenaConexion = cadenaConexion;
    }

    /**
     * @return the cadenaSQL
     */
    public String getCadenaSQL() {
        return cadenaSQL;
    }

    /**
     * @param cadenaSQL the cadenaSQL to set
     */
    public void setCadenaSQL(String cadenaSQL) {
        this.cadenaSQL = cadenaSQL;
    }

    /**
     * @return the esSelect
     */
    public boolean isEsSelect() {
        return esSelect;
    }

    /**
     * @param esSelect the esSelect to set
     */
    public void setEsSelect(boolean esSelect) {
        this.esSelect = esSelect;
    }

    /**
     * @return the dbConnection
     */
    public Connection getDbConnection() {
        return dbConnection;
    }

    /**
     * @param dbConnection the dbConnection to set
     */
    public void setDbConnection(Connection dbConnection) {
        this.dbConnection = dbConnection;
    }

    /**
     * @return the dbResultSet
     */
    public ResultSet getDbResultSet() {
        return dbResultSet;
    }

    /**
     * @param dbResultSet the dbResultSet to set
     */
    public void setDbResultSet(ResultSet dbResultSet) {
        this.dbResultSet = dbResultSet;
    }

    /**
     * @return the usuario
     */
    public String getUsuario() {
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    /**
     * @return the pass
     */
    public String getPass() {
        return pass;
    }

    /**
     * @param pass the pass to set
     */
    public void setPass(String pass) {
        this.pass = pass;
    }
    
    
    public void cerrar()
    {
       try
       {
          this.getDbConnection().close();
       }
       catch(Exception ex)
       {
          JOptionPane.showMessageDialog(null, "Error al cerrar " + ex.getMessage());
       }
    
    } //Fin Cerrar
    
    public void conectar()
    {
        if (this.getNombreBaseDatos().length() == 0)
        {
            JOptionPane.showMessageDialog(null, "Falta nombre base de datos ");
            return;
        }

        if (this.getNombreTabla().length() == 0)
        {
            JOptionPane.showMessageDialog(null, "Falta nombre tabla ");
            return;
        }
        
        if (this.getCadenaConexion().length() == 0)
        {
            JOptionPane.showMessageDialog(null, "Falta cadena conexion");
            return;
        }

        if (this.getCadenaSQL().length() == 0)
        {
            JOptionPane.showMessageDialog(null, "Falta cadena SQL ");
            return;
        }

        //Se instancia la conexion
        
        Statement st = null;
        try
        {
            //Se carga el Driver
            Class.forName(this.getCadenaConexion());
            //Se carga conexion
            this.setDbConnection(DriverManager.getConnection(this.getNombreBaseDatos(), 
                                                             this.getUsuario(), 
                                                             this.getPass()));
            st = this.getDbConnection().createStatement();
        }
        catch(Exception ex)
        {
            JOptionPane.showMessageDialog(null, "Problema de Conexion " + ex.getMessage());
            return;
        
        }
        
        if (this.isEsSelect()) //SELECT
        {
            //Se carga el resultSet
            try
            {
                this.setDbResultSet(st.executeQuery(this.getCadenaSQL()));
            }
            catch(Exception ex)
            {
               JOptionPane.showMessageDialog(null, "Problema al cargar ResultSet " + ex.getMessage());
               return;
            
            }
        }
        else //INSERT - UPDATE - DELETE
        {
            try
            {
                int insertFila = st.executeUpdate(this.getCadenaSQL());
            }
            catch(Exception ex)
            {
            
            }
        }

        
       //this.cerrar();
        
        
    } //Fin conectar
    
//    public static void main(String[] args)
//    {
//       ConexionMySql conec = new ConexionMySql();
//       conec.setNombreBaseDatos("jdbc:mysql://localhost/chutealapp");
//       conec.setNombreTabla("cancha");
//       conec.setCadenaConexion("com.mysql.jdbc.Driver");
//       conec.setUsuario("root");
//       conec.setPass("");
//       conec.setCadenaSQL("INSERT INTO cancha (id_cancha,codigo_comuna,nombre_cancha,fono_cancha,precio_cancha, direccion) "
//               + "VALUES (null,329, 'Cancha Recoleta', '+56223441523', 5000, 'Av. Peru 365');");
//       conec.setEsSelect(false);
//       conec.conectar();
//
//    }
    
    
    
    
    
    
}
