/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CapaDTO;

/**
 *
 * @author Andres
 */
public class Region {
    private int codigo_region;
    private String nombre;

    /**
     * @return the codigo_region
     */
    public int getCodigo_region() {
        return codigo_region;
    }

    /**
     * @param codigo_region the codigo_region to set
     */
    public void setCodigo_region(int codigo_region) {
        this.codigo_region = codigo_region;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
