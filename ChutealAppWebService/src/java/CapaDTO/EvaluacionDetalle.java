/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CapaDTO;

/**
 *
 * @author Maltra
 */
public class EvaluacionDetalle {
    
    private int id_eval_detalle;
    private int id_cancha;
    private int id_equipo;
    private int id_eval_cabecera;
    private int id_jugador;
    private int id_escala;
    private String comentario;

    /**
     * @return the id_eval_detalle
     */
    public int getId_eval_detalle() {
        return id_eval_detalle;
    }

    /**
     * @param id_eval_detalle the id_eval_detalle to set
     */
    public void setId_eval_detalle(int id_eval_detalle) {
        this.id_eval_detalle = id_eval_detalle;
    }

    /**
     * @return the id_cancha
     */
    public int getId_cancha() {
        return id_cancha;
    }

    /**
     * @param id_cancha the id_cancha to set
     */
    public void setId_cancha(int id_cancha) {
        this.id_cancha = id_cancha;
    }

    /**
     * @return the id_equipo
     */
    public int getId_equipo() {
        return id_equipo;
    }

    /**
     * @param id_equipo the id_equipo to set
     */
    public void setId_equipo(int id_equipo) {
        this.id_equipo = id_equipo;
    }

    /**
     * @return the id_eval_cabecera
     */
    public int getId_eval_cabecera() {
        return id_eval_cabecera;
    }

    /**
     * @param id_eval_cabecera the id_eval_cabecera to set
     */
    public void setId_eval_cabecera(int id_eval_cabecera) {
        this.id_eval_cabecera = id_eval_cabecera;
    }

    /**
     * @return the id_jugador
     */
    public int getId_jugador() {
        return id_jugador;
    }

    /**
     * @param id_jugador the id_jugador to set
     */
    public void setId_jugador(int id_jugador) {
        this.id_jugador = id_jugador;
    }

    /**
     * @return the id_escala
     */
    public int getId_escala() {
        return id_escala;
    }

    /**
     * @param id_escala the id_escala to set
     */
    public void setId_escala(int id_escala) {
        this.id_escala = id_escala;
    }

    /**
     * @return the comentario
     */
    public String getComentario() {
        return comentario;
    }

    /**
     * @param comentario the comentario to set
     */
    public void setComentario(String comentario) {
        this.comentario = comentario;
    }
}
