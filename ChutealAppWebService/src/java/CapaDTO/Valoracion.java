/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CapaDTO;

/**
 *
 * @author Andres
 */
public class Valoracion {
    private int id_escala;
    private double valoracion;

    /**
     * @return the id_escala
     */
    public int getId_escala() {
        return id_escala;
    }

    /**
     * @param id_escala the id_escala to set
     */
    public void setId_escala(int id_escala) {
        this.id_escala = id_escala;
    }

    /**
     * @return the valoracion
     */
    public double getValoracion() {
        return valoracion;
    }

    /**
     * @param valoracion the valoracion to set
     */
    public void setValoracion(double valoracion) {
        this.valoracion = valoracion;
    }
    
}
