/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CapaDTO;

/**
 *
 * @author Andres
 */
public class Equipo {
    private int id_equipo;
    private String nombre;
    private String nombre_contacto;
    private String fono_contacto;
    private String email_contacto;
    private int id_comuna;

    /**
     * @return the id_equipo
     */
    public int getId_equipo() {
        return id_equipo;
    }

    /**
     * @param id_equipo the id_equipo to set
     */
    public void setId_equipo(int id_equipo) {
        this.id_equipo = id_equipo;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the nombre_contacto
     */
    public String getNombre_contacto() {
        return nombre_contacto;
    }

    /**
     * @param nombre_contacto the nombre_contacto to set
     */
    public void setNombre_contacto(String nombre_contacto) {
        this.nombre_contacto = nombre_contacto;
    }

    /**
     * @return the fono_contacto
     */
    public String getFono_contacto() {
        return fono_contacto;
    }

    /**
     * @param fono_contacto the fono_contacto to set
     */
    public void setFono_contacto(String fono_contacto) {
        this.fono_contacto = fono_contacto;
    }

    /**
     * @return the email_contacto
     */
    public String getEmail_contacto() {
        return email_contacto;
    }

    /**
     * @param email_contacto the email_contacto to set
     */
    public void setEmail_contacto(String email_contacto) {
        this.email_contacto = email_contacto;
    }

    /**
     * @return the id_comuna
     */
    public int getId_comuna() {
        return id_comuna;
    }

    /**
     * @param id_comuna the id_comuna to set
     */
    public void setId_comuna(int id_comuna) {
        this.id_comuna = id_comuna;
    }
}
