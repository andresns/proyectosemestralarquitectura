/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CapaDTO;

/**
 *
 * @author Andres
 */
public class Disponibilidad {
    private int id_disponibilidad;
    private int id_equipo;
    private int id_cancha;
    private int id_dia;
    private String hora_inicio;
    private String hora_termino;

    /**
     * @return the id_disponibilidad
     */
    public int getId_disponibilidad() {
        return id_disponibilidad;
    }

    /**
     * @param id_disponibilidad the id_disponibilidad to set
     */
    public void setId_disponibilidad(int id_disponibilidad) {
        this.id_disponibilidad = id_disponibilidad;
    }

    /**
     * @return the id_equipo
     */
    public int getId_equipo() {
        return id_equipo;
    }

    /**
     * @param id_equipo the id_equipo to set
     */
    public void setId_equipo(int id_equipo) {
        this.id_equipo = id_equipo;
    }

    /**
     * @return the id_cancha
     */
    public int getId_cancha() {
        return id_cancha;
    }

    /**
     * @param id_cancha the id_cancha to set
     */
    public void setId_cancha(int id_cancha) {
        this.id_cancha = id_cancha;
    }

    /**
     * @return the id_dia
     */
    public int getId_dia() {
        return id_dia;
    }

    /**
     * @param id_dia the id_dia to set
     */
    public void setId_dia(int id_dia) {
        this.id_dia = id_dia;
    }

    /**
     * @return the hora_inicio
     */
    public String getHora_inicio() {
        return hora_inicio;
    }

    /**
     * @param hora_inicio the hora_inicio to set
     */
    public void setHora_inicio(String hora_inicio) {
        this.hora_inicio = hora_inicio;
    }

    /**
     * @return the hora_termino
     */
    public String getHora_termino() {
        return hora_termino;
    }

    /**
     * @param hora_termino the hora_termino to set
     */
    public void setHora_termino(String hora_termino) {
        this.hora_termino = hora_termino;
    }
}
