/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CapaDTO;

/**
 *
 * @author Andres
 */
public class EvaluacionCabecera {
    private int id_evaluacion_cabecera;
    private int id_jugador;

    /**
     * @return the id_evaluacion_cabecera
     */
    public int getId_evaluacion_cabecera() {
        return id_evaluacion_cabecera;
    }

    /**
     * @param id_evaluacion_cabecera the id_evaluacion_cabecera to set
     */
    public void setId_evaluacion_cabecera(int id_evaluacion_cabecera) {
        this.id_evaluacion_cabecera = id_evaluacion_cabecera;
    }

    /**
     * @return the id_jugador
     */
    public int getId_jugador() {
        return id_jugador;
    }

    /**
     * @param id_jugador the id_jugador to set
     */
    public void setId_jugador(int id_jugador) {
        this.id_jugador = id_jugador;
    }
}
