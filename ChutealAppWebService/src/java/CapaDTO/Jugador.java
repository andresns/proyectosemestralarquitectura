/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CapaDTO;

import java.util.Date;

/**
 *
 * @author Andres
 */
public class Jugador {
    private int id_jugador;
    private String nombres;
    private String apellidos;
    private String nombre_usuario;
    private String contrasena;
    private String email;
    private String telefono;
    private String fecha_nacimiento;
    private int id_posicion;
    private int id_comuna;

    /**
     * @return the id_jugador
     */
    public int getId_jugador() {
        return id_jugador;
    }

    /**
     * @param id_jugador the id_jugador to set
     */
    public void setId_jugador(int id_jugador) {
        this.id_jugador = id_jugador;
    }

    /**
     * @return the nombres
     */
    public String getNombres() {
        return nombres;
    }

    /**
     * @param nombres the nombres to set
     */
    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    /**
     * @return the apellidos
     */
    public String getApellidos() {
        return apellidos;
    }

    /**
     * @param apellidos the apellidos to set
     */
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    /**
     * @return the nombre_usuario
     */
    public String getNombre_usuario() {
        return nombre_usuario;
    }

    /**
     * @param nombre_usuario the nombre_usuario to set
     */
    public void setNombre_usuario(String nombre_usuario) {
        this.nombre_usuario = nombre_usuario;
    }

    /**
     * @return the contrasena
     */
    public String getContrasena() {
        return contrasena;
    }

    /**
     * @param contrasena the contrasena to set
     */
    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the telefono
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     * @param telefono the telefono to set
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    /**
     * @return the fecha_nacimiento
     */
    public String getFecha_nacimiento() {
        return fecha_nacimiento;
    }

    /**
     * @param fecha_nacimiento the fecha_nacimiento to set
     */
    public void setFecha_nacimiento(String fecha_nacimiento) {
        this.fecha_nacimiento = fecha_nacimiento;
    }

    /**
     * @return the id_posicion
     */
    public int getId_posicion() {
        return id_posicion;
    }

    /**
     * @param id_posicion the id_posicion to set
     */
    public void setId_posicion(int id_posicion) {
        this.id_posicion = id_posicion;
    }

    /**
     * @return the id_comuna
     */
    public int getId_comuna() {
        return id_comuna;
    }

    /**
     * @param id_comuna the id_comuna to set
     */
    public void setId_comuna(int id_comuna) {
        this.id_comuna = id_comuna;
    }

}
