/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CapaDTO;

/**
 *
 * @author Maltra
 */
public class TipoPartido {
    
    private int id_tipo_partido;
    private String descripcion;

    /**
     * @return the id_tipo_partido
     */
    public int getId_tipo_partido() {
        return id_tipo_partido;
    }

    /**
     * @param id_tipo_partido the id_tipo_partido to set
     */
    public void setId_tipo_partido(int id_tipo_partido) {
        this.id_tipo_partido = id_tipo_partido;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
}
