/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CapaDTO;

/**
 *
 * @author Andres
 */
public class Comuna {
    private int codigo_comuna;
    private String nombre;
    private int codigo_region;

    
    /**
     * @return the codigo_comuna
     */
    public int getCodigo_comuna() {
        return codigo_comuna;
    }

    /**
     * @param codigo_comuna the codigo_comuna to set
     */
    public void setCodigo_comuna(int codigo_comuna) {
        this.codigo_comuna = codigo_comuna;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the codigo_region
     */
    public int getCodigo_region() {
        return codigo_region;
    }

    /**
     * @param codigo_region the codigo_region to set
     */
    public void setCodigo_region(int codigo_region) {
        this.codigo_region = codigo_region;
    }
}
