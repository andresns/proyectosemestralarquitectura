/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CapaDTO;

/**
 *
 * @author Andres
 */
public class Dia {
    private int id_dia;
    private String nombre_dia;

    /**
     * @return the id_dia
     */
    public int getId_dia() {
        return id_dia;
    }

    /**
     * @param id_dia the id_dia to set
     */
    public void setId_dia(int id_dia) {
        this.id_dia = id_dia;
    }

    /**
     * @return the nombre_dia
     */
    public String getNombre_dia() {
        return nombre_dia;
    }

    /**
     * @param nombre_dia the nombre_dia to set
     */
    public void setNombre_dia(String nombre_dia) {
        this.nombre_dia = nombre_dia;
    }
    
}
