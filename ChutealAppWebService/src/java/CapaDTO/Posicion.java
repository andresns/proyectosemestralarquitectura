/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CapaDTO;

/**
 *
 * @author Andres
 */
public class Posicion {
    private int id_posicion;
    private String nombre_posicion;

    /**
     * @return the id_posicion
     */
    public int getId_posicion() {
        return id_posicion;
    }

    /**
     * @param id_posicion the id_posicion to set
     */
    public void setId_posicion(int id_posicion) {
        this.id_posicion = id_posicion;
    }

    /**
     * @return the nombre_posicion
     */
    public String getNombre_posicion() {
        return nombre_posicion;
    }

    /**
     * @param nombre_posicion the nombre_posicion to set
     */
    public void setNombre_posicion(String nombre_posicion) {
        this.nombre_posicion = nombre_posicion;
    }
}
