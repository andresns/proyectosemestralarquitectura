/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CapaDTO;

/**
 *
 * @author Andres
 */
public class Solicitud {
    private int id_solicitud;
    private int id_equipo_1;
    private int id_equipo_2;
    private int id_jugador_1;
    private int id_jugador_2;
    private int estado;

    /**
     * @return the id_solicitud
     */
    public int getId_solicitud() {
        return id_solicitud;
    }

    /**
     * @param id_solicitud the id_solicitud to set
     */
    public void setId_solicitud(int id_solicitud) {
        this.id_solicitud = id_solicitud;
    }

    /**
     * @return the id_equipo_1
     */
    public int getId_equipo_1() {
        return id_equipo_1;
    }

    /**
     * @param id_equipo_1 the id_equipo_1 to set
     */
    public void setId_equipo_1(int id_equipo_1) {
        this.id_equipo_1 = id_equipo_1;
    }

    /**
     * @return the id_equipo_2
     */
    public int getId_equipo_2() {
        return id_equipo_2;
    }

    /**
     * @param id_equipo_2 the id_equipo_2 to set
     */
    public void setId_equipo_2(int id_equipo_2) {
        this.id_equipo_2 = id_equipo_2;
    }

    /**
     * @return the id_jugador_1
     */
    public int getId_jugador_1() {
        return id_jugador_1;
    }

    /**
     * @param id_jugador_1 the id_jugador_1 to set
     */
    public void setId_jugador_1(int id_jugador_1) {
        this.id_jugador_1 = id_jugador_1;
    }

    /**
     * @return the id_jugador_2
     */
    public int getId_jugador_2() {
        return id_jugador_2;
    }

    /**
     * @param id_jugador_2 the id_jugador_2 to set
     */
    public void setId_jugador_2(int id_jugador_2) {
        this.id_jugador_2 = id_jugador_2;
    }

    /**
     * @return the estado
     */
    public int getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(int estado) {
        this.estado = estado;
    }

    
}
