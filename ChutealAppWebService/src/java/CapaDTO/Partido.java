/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CapaDTO;

/**
 *
 * @author Maltra
 */
public class Partido {
    
    private int id_partido;
    private int id_cancha;
    private int id_tipo_partido;
    private int id_equipo_1;
    private int id_equipo_2;

    /**
     * @return the id_partido
     */
    public int getId_partido() {
        return id_partido;
    }

    /**
     * @param id_partido the id_partido to set
     */
    public void setId_partido(int id_partido) {
        this.id_partido = id_partido;
    }

    /**
     * @return the id_cancha
     */
    public int getId_cancha() {
        return id_cancha;
    }

    /**
     * @param id_cancha the id_cancha to set
     */
    public void setId_cancha(int id_cancha) {
        this.id_cancha = id_cancha;
    }

    /**
     * @return the id_tipo_partido
     */
    public int getId_tipo_partido() {
        return id_tipo_partido;
    }

    /**
     * @param id_tipo_partido the id_tipo_partido to set
     */
    public void setId_tipo_partido(int id_tipo_partido) {
        this.id_tipo_partido = id_tipo_partido;
    }

    /**
     * @return the id_equipo_1
     */
    public int getId_equipo_1() {
        return id_equipo_1;
    }

    /**
     * @param id_equipo_1 the id_equipo_1 to set
     */
    public void setId_equipo_1(int id_equipo_1) {
        this.id_equipo_1 = id_equipo_1;
    }

    /**
     * @return the id_equipo_2
     */
    public int getId_equipo_2() {
        return id_equipo_2;
    }

    /**
     * @param id_equipo_2 the id_equipo_2 to set
     */
    public void setId_equipo_2(int id_equipo_2) {
        this.id_equipo_2 = id_equipo_2;
    }
    
}
