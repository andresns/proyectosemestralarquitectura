/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CapaServicios;

import CapaNegocio.NegocioEvaluacionDetalle;
import CapaDTO.EvaluacionDetalle;
import CapaDTO.Partido;
import CapaDTO.Cancha;
import CapaDTO.Comuna;
import CapaDTO.Dia;
import CapaDTO.Disponibilidad;
import CapaDTO.Equipo;
import CapaDTO.EvaluacionCabecera;
import CapaDTO.Jugador;
import CapaDTO.Posicion;
import CapaDTO.Region;
import CapaDTO.Sesion;
import CapaDTO.Solicitud;
import CapaDTO.TipoPartido;
import CapaDTO.Valoracion;
import CapaNegocio.NegocioCancha;
import CapaNegocio.NegocioComuna;
import CapaNegocio.NegocioDia;
import CapaNegocio.NegocioDisponibilidad;
import CapaNegocio.NegocioPartido;
import CapaNegocio.NegocioEquipo;
import CapaNegocio.NegocioEvaluacionCabecera;
import CapaNegocio.NegocioJugador;
import CapaNegocio.NegocioJugadorEquipo;
import CapaNegocio.NegocioPosicion;
import CapaNegocio.NegocioRegion;
import CapaNegocio.NegocioSolicitud;
import CapaNegocio.NegocioTipoPartido;
import CapaNegocio.NegocioValoracion;
import java.util.ArrayList;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author Andres
 */
@WebService(serviceName = "WebServiceChutealApp")
public class WebServiceChutealApp {

    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "registrarUsuario")
    public void registrarUsuario(@WebParam(name = "jugador") Jugador jugador) {
        NegocioJugador auxNegocio = new NegocioJugador();
        auxNegocio.insertarJugador(jugador);
    }
    
    @WebMethod(operationName = "iniciarSesion")
    public boolean iniciarSesion(@WebParam(name = "nombreUsuario") String nombreUsuario,
                                @WebParam(name = "contrasena") String contrasena) {
        NegocioJugador auxNegocio = new NegocioJugador();
        Jugador auxJugador = new Jugador();
        
        auxJugador = auxNegocio.buscarJugadorNombreUsuario(nombreUsuario);
        
        if(auxJugador.getNombre_usuario().equals(nombreUsuario) &&
           auxJugador.getContrasena().equals(contrasena))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    @WebMethod(operationName = "creaEquipo")
    public void crearEquipo(@WebParam(name = "equipo") Equipo equipo) {
        NegocioEquipo auxNegocio = new NegocioEquipo();
        
        auxNegocio.insertarEquipo(equipo);;
    }
    
    @WebMethod(operationName = "buscarEquipoParaUnirse")
    public ArrayList<Equipo> buscarEquipoParaUnirse(@WebParam(name = "nombreEquipo") String nombreEquipo,
                               @WebParam(name = "idComuna") int idComuna) {
        ArrayList<Equipo> auxArrayEquipo = new ArrayList<>();
        NegocioEquipo auxNegocio = new NegocioEquipo();

        if(!nombreEquipo.equals("")){
            Equipo auxEquipo = new Equipo();
            auxEquipo = auxNegocio.buscarEquipoNombre(nombreEquipo);
            auxArrayEquipo.add(auxEquipo);
        }else{
            auxArrayEquipo = auxNegocio.listarEquiposComuna(idComuna);
        }
        return auxArrayEquipo;
    }
    
    @WebMethod(operationName = "buscarJugadores")
    public ArrayList<Jugador> buscarJugadores() {
        NegocioJugador auxNegocio = new NegocioJugador();
        ArrayList<Jugador> auxListaJugadores = new ArrayList<>();
        auxListaJugadores = auxNegocio.listarJugadores();
        return auxListaJugadores;
    }
    
    @WebMethod(operationName = "buscarEquipoRival")
    public ArrayList<Equipo> buscarEquipoRival(@WebParam(name = "disponibilidad") Disponibilidad disponibilidad,
                                               @WebParam(name = "comuna") Comuna comuna) {
        ArrayList<Equipo> auxListaEquipos = new ArrayList<>();
        NegocioEquipo auxNegocio = new NegocioEquipo();
        
        auxListaEquipos = auxNegocio.listarEquiposRivales(disponibilidad, comuna);
        
        return auxListaEquipos;
    }
    
    @WebMethod(operationName = "enviarSolicitud")
    public void enviarSolicitud(@WebParam(name = "solicitud") Solicitud solicitud) {
        NegocioSolicitud auxNegocio = new NegocioSolicitud();
        auxNegocio.insertarSolicitud(solicitud);
    }
    
    @WebMethod(operationName = "notificarJugador")
    public ArrayList<Solicitud> notificarJugador() {
        ArrayList<Solicitud> auxListaSolicitudes = new ArrayList<>();
        
        return auxListaSolicitudes;
    }
    
    @WebMethod(operationName = "buscarCanchas")
    public ArrayList<Cancha> buscarCanchas(@WebParam(name = "idComunas") int[] idComunas) {
        ArrayList<Cancha> auxListaCanchas = new ArrayList<>();
        
        return auxListaCanchas;
    }
    
    @WebMethod(operationName = "buscarDatosContacto")
    public Equipo buscarDatosContacto(@WebParam(name = "nombreEquipo") String nombreEquipo) {
        NegocioEquipo auxNegocio = new NegocioEquipo();
        Equipo auxEquipo = new Equipo();
        
        auxEquipo = auxNegocio.buscarEquipoNombre(nombreEquipo);
        
        return auxEquipo;
    }
    
    @WebMethod(operationName = "registrarPartido")
    public void registrarPartido(@WebParam(name = "partido") Partido partido) {
        NegocioPartido auxNegocio = new NegocioPartido();
        auxNegocio.insertarPartido(partido);
        
    }
    
    @WebMethod(operationName = "registrarComentario")
    public void registrarComentario(@WebParam(name = "evaluacion_detalle") EvaluacionDetalle evaluacion_detalle){
        EvaluacionDetalle auxEvaluacion = new EvaluacionDetalle();
        NegocioEvaluacionDetalle auxNegocio = new NegocioEvaluacionDetalle();
        
        auxEvaluacion = evaluacion_detalle;
        if(auxEvaluacion.getId_jugador()!=0){
            auxNegocio.insertarComentarioJugador(auxEvaluacion);
        }else if(auxEvaluacion.getId_equipo()!=0){
            auxNegocio.insertarComentarioEquipo(auxEvaluacion);
        }else{
            auxNegocio.insertarComentarioCancha(auxEvaluacion);
        }
    }
    
    @WebMethod(operationName = "buscarRegionNombre")
    public Region buscarRegionNombre(@WebParam(name = "nombre_region") String nombre_region) {
        NegocioRegion auxNegocio = new NegocioRegion();
        Region auxRegion = new Region();
        auxRegion = auxNegocio.buscarRegionNombre(nombre_region);

        return auxRegion;
    }
    
    @WebMethod(operationName = "buscarRegionId")
    public Region buscarRegionId(@WebParam(name = "codigo_region") int codigo_region) {
        NegocioRegion auxNegocio = new NegocioRegion();
        Region auxRegion = new Region();
        auxRegion = auxNegocio.buscarRegionId(codigo_region);

        return auxRegion;
    }
    
    @WebMethod(operationName = "listarRegiones")
    public ArrayList<Region> listarRegiones() {
        NegocioRegion auxNegocio = new NegocioRegion();
        ArrayList<Region> auxListaRegiones = new ArrayList<>();
        
        auxListaRegiones = auxNegocio.listarRegiones();
        
        return auxListaRegiones;
    }

    @WebMethod(operationName = "listarComunas")
    public ArrayList<Comuna> listarComunas(@WebParam(name = "codigo_region") int codigo_region) {
        NegocioComuna auxNegocio = new NegocioComuna();
        ArrayList<Comuna> auxListaComunas = new ArrayList<>();

        auxListaComunas = auxNegocio.listarComunas(codigo_region);

        return auxListaComunas;
    }
    
    @WebMethod(operationName = "buscarComunaNombre")
    public Comuna buscarComunaNombre(@WebParam(name = "nombre_comuna") String nombre_comuna) {
        NegocioComuna auxNegocio = new NegocioComuna();
        Comuna auxComuna = new Comuna();
        auxComuna = auxNegocio.buscarComunaNombre(nombre_comuna);

        return auxComuna;
    }
    
    @WebMethod(operationName = "buscarComunaId")
    public Comuna buscarComunaId(@WebParam(name = "codigo_comuna") int codigo_comuna) {
        NegocioComuna auxNegocio = new NegocioComuna();
        Comuna auxComuna = new Comuna();
        auxComuna = auxNegocio.buscarComunaId(codigo_comuna);

        return auxComuna;
    }
    
    @WebMethod(operationName = "buscarPosicionId")
    public Posicion buscarPosicionId(@WebParam(name = "id_posicion") int id_posicion) {
        NegocioPosicion auxNegocio = new NegocioPosicion();
        Posicion auxPosicion = new Posicion();
        auxPosicion = auxNegocio.buscarPosicionId(id_posicion);

        return auxPosicion;
    }
    
    @WebMethod(operationName = "buscarPosicionNombre")
    public Posicion buscarPosicionNombre(@WebParam(name = "nombre_posicion") String nombre_posicion) {
        NegocioPosicion auxNegocio = new NegocioPosicion();
        Posicion auxPosicion = new Posicion();
        auxPosicion = auxNegocio.buscarPosicionNombre(nombre_posicion);

        return auxPosicion;
    }
    
    @WebMethod(operationName = "listarPosiciones")
    public ArrayList<Posicion> listarPosiciones() {
        NegocioPosicion auxNegocio = new NegocioPosicion();
        ArrayList<Posicion> auxListaPosiciones = new ArrayList<>();

        auxListaPosiciones = auxNegocio.listarPosiciones();

        return auxListaPosiciones;
    }
    
    @WebMethod(operationName = "idUltimoEquipo")
    public int idUltimoEquipo() {
        NegocioEquipo auxNegocio = new NegocioEquipo();

        int idUltimoEquipo = auxNegocio.idUltimoEquipo();

        return idUltimoEquipo;
    }
    
    @WebMethod(operationName = "agregarJugadorEquipo")
    public void agregarJugadorEquipo(@WebParam(name = "id_jugador") int id_jugador,
                                     @WebParam(name = "id_equipo") int id_equipo) {
        NegocioJugadorEquipo auxNegocio = new NegocioJugadorEquipo();
        auxNegocio.insertarJugadorEquipo(id_jugador, id_equipo);
    }
    
    @WebMethod(operationName = "buscarJugadorNombreUsuario")
    public Jugador buscarJugadorNombreUsuario(@WebParam(name = "nombreJugadorUsuario") String nombreJugadorUsuario) {
        NegocioJugador auxNegocio = new NegocioJugador();
        Jugador auxJugador = new Jugador();
        auxJugador = auxNegocio.buscarJugadorNombreUsuario(nombreJugadorUsuario);
        
        return auxJugador;
    }
    
    @WebMethod(operationName = "buscarJugadorId")
    public Jugador buscarJugadorId(@WebParam(name = "idJugador") int idJugador) {
        NegocioJugador auxNegocio = new NegocioJugador();
        Jugador auxJugador = new Jugador();
        auxJugador = auxNegocio.buscarJugador(idJugador);
        
        return auxJugador;
    }
    
    @WebMethod(operationName = "eliminarJugador")
    public void eliminarJugador(@WebParam(name = "id_jugador") int id_jugador) {
        NegocioJugador auxNegocio = new NegocioJugador();
        auxNegocio.eliminarJugador(id_jugador);
    }
    
    @WebMethod(operationName = "modificarJugador")
    public void modificarJugador(@WebParam(name = "jugador") Jugador jugador) {
        NegocioJugador auxNegocio = new NegocioJugador();
        auxNegocio.actualizarJugador(jugador);
    }
    
    @WebMethod(operationName = "controlSesion")
    public Sesion controlSesion(@WebParam(name = "jugador") Jugador jugador) {
        Sesion auxSesion = new Sesion();
        auxSesion.setJugador(jugador);
        
        return auxSesion;
    }
    
    @WebMethod(operationName = "buscarJugadorEnEquipo")
    public Equipo buscarJugadorEnEquipo(@WebParam(name = "jugador") Jugador jugador) {
        Equipo auxEquipo = new Equipo();
        NegocioJugadorEquipo auxNegocioJugadorEquipo = new NegocioJugadorEquipo();
        auxEquipo = auxNegocioJugadorEquipo.buscarJugadorEnEquipo(jugador);
        
        return auxEquipo;
    }
    
    @WebMethod(operationName = "listarEquiposDeJugador")
    public ArrayList<Equipo> listarEquiposDeJugador(@WebParam(name = "jugador") Jugador jugador) {
        NegocioEquipo auxNegocio = new NegocioEquipo();
        ArrayList<Equipo> auxListaEquipos = new ArrayList<>();
        auxListaEquipos = auxNegocio.listarEquiposDeJugador(jugador);
        return auxListaEquipos;
    }
    
    @WebMethod(operationName = "obtenerValoracionEquipo")
    public Double obtenerValoracionEquipo(@WebParam(name = "equipo") Equipo equipo) {
        ArrayList<Valoracion> auxListaValoraciones = new ArrayList<>();
        NegocioEvaluacionDetalle auxNegocio = new NegocioEvaluacionDetalle();
        auxListaValoraciones = auxNegocio.obtenerValoracionEquipo(equipo);
        
        double sumaValoraciones = 0;
        double promedioValoraciones = 0;
        
        for(Valoracion valoracion : auxListaValoraciones){
            sumaValoraciones = sumaValoraciones + valoracion.getValoracion();
        }
        
        if(auxListaValoraciones.size()>0){
            promedioValoraciones = sumaValoraciones/auxListaValoraciones.size();
        }
        
        return promedioValoraciones;
    }
    
    @WebMethod(operationName = "obtenerValoracion")
    public Valoracion obtenerValoracion(@WebParam(name = "id_escala") int id_escala) {
        Valoracion auxValoracion = new Valoracion();
        NegocioValoracion auxNegocio = new NegocioValoracion();
        auxValoracion = auxNegocio.buscarValoracion(id_escala);
        
        return auxValoracion;
    }
    
    @WebMethod(operationName = "obtenerComentarios")
    public ArrayList<EvaluacionDetalle> obtenerComentarios(@WebParam(name = "equipo") Equipo equipo,
                                                           @WebParam(name = "jugador") Jugador jugador,
                                                           @WebParam(name = "cancha") Cancha cancha) {
        ArrayList<EvaluacionDetalle> auxListaEvaluacionDetalle = new ArrayList<>();
        
        NegocioEvaluacionDetalle auxNegocio = new NegocioEvaluacionDetalle();
        
        if(equipo!=null){
            //Obtener Valoracion de Equipo
            auxListaEvaluacionDetalle = auxNegocio.obtenerEvaluacionDetalleEquipo(equipo);
        }else if(jugador!=null){
            //Obtener Valoracion de Jugador
            auxListaEvaluacionDetalle = auxNegocio.obtenerEvaluacionDetalleJugador(jugador);
        }else{
            //Obtener Valoracion de Cancha
            auxListaEvaluacionDetalle = auxNegocio.obtenerEvaluacionDetalleCancha(cancha);
        }

        return auxListaEvaluacionDetalle;
    }
    
    @WebMethod(operationName = "obtenerEvaluacionCabecera")
    public EvaluacionCabecera obtenerEvaluacionCabecera(@WebParam(name = "id_evaluacion_cabecera") int id_evaluacion_cabecera) {
        EvaluacionCabecera auxEvaluacionCabecera = new EvaluacionCabecera();
        NegocioEvaluacionCabecera auxNegocio = new NegocioEvaluacionCabecera();
        
        auxEvaluacionCabecera = auxNegocio.obtenerEvaluacionCabecera(id_evaluacion_cabecera);

        return auxEvaluacionCabecera;
    }
    
    @WebMethod(operationName = "listaJugadoresDeEquipo")
    public ArrayList<Jugador> listaJugadoresDeEquipo(@WebParam(name = "equipo") Equipo equipo) {
        NegocioJugadorEquipo auxNegocio = new NegocioJugadorEquipo();
        ArrayList<Jugador> auxArrayList = new ArrayList<>();
        auxArrayList = auxNegocio.listarJugadoresDeEquipo(equipo);
        return auxArrayList;
    }
    
    @WebMethod(operationName = "insertarDia")
    public void insertarDia(@WebParam(name = "dia") Dia dia) {
        NegocioDia auxNegocio = new NegocioDia();
        auxNegocio.insertarDia(dia);
    }
    
    @WebMethod(operationName = "listarDias")
    public ArrayList<Dia> listarDias() {
        NegocioDia auxNegocio = new NegocioDia();
        ArrayList<Dia> auxListaDias = new ArrayList<>();
        auxListaDias = auxNegocio.listarDias();
        return auxListaDias;
    }
    
    @WebMethod(operationName = "buscarDiaId")
    public Dia buscarDiaId(@WebParam(name = "id_dia") int id_dia) {
        NegocioDia auxNegocio = new NegocioDia();
        Dia auxDia = new Dia();
        auxDia = auxNegocio.buscarDiaId(id_dia);
        return auxDia;
    }
    
    @WebMethod(operationName = "buscarDiaNombre")
    public Dia buscarDiaNombre(@WebParam(name = "nombre_dia") String nombre_dia) {
        NegocioDia auxNegocio = new NegocioDia();
        Dia auxDia = new Dia();
        auxDia = auxNegocio.buscarDiaNombre(nombre_dia);
        return auxDia;
    }
    
    @WebMethod(operationName = "eliminarDia")
    public void eliminarDia(@WebParam(name = "id_dia") int id_dia) {
        NegocioDia auxNegocio = new NegocioDia();
        auxNegocio.eliminarDia(id_dia);
    }
    
    @WebMethod(operationName = "modificarDia")
    public void modificarDia(@WebParam(name = "dia") Dia dia) {
        NegocioDia auxNegocio = new NegocioDia();
        auxNegocio.modificarDia(dia);
    }
    
    @WebMethod(operationName = "ListarDisponibilidades")
    public ArrayList<Disponibilidad> ListarDisponibilidadEquipo(@WebParam(name = "equipo") Equipo equipo) {
        NegocioDisponibilidad auxNegocio = new NegocioDisponibilidad();
        
        ArrayList<Disponibilidad> auxListaDisponibilidad = new ArrayList<>();
        auxListaDisponibilidad = auxNegocio.obtenerDisponibilidadEquipo(equipo);
        
        return auxListaDisponibilidad;
    }
    
    @WebMethod(operationName = "eliminarEquipo")
    public void eliminarEquipo(@WebParam(name = "id_equipo") int id_equipo) {
        NegocioEquipo auxNegocio = new NegocioEquipo();
        auxNegocio.eliminarEquipo(id_equipo);
    }
    
    @WebMethod(operationName = "eliminarJugadorDeEquipo")
    public void eliminarJugadorDeEquipo(@WebParam(name = "id_jugador") int id_jugador,
                                        @WebParam(name = "id_equipo") int id_equipo) {
        NegocioJugadorEquipo auxNegocio = new NegocioJugadorEquipo();
        auxNegocio.eliminarJugadorDeEquipo(id_jugador, id_equipo);
    }
    
    @WebMethod(operationName = "actualizaEquipo")
    public void actualizaEquipo(@WebParam(name = "equipo") Equipo equipo) {
        NegocioEquipo auxNegocio = new NegocioEquipo();
        auxNegocio.actualizarEquipo(equipo);
    }
    
    @WebMethod(operationName = "insertaPosicion")
    public void insertaPosicion(@WebParam(name = "posicion") Posicion posicion) {
        NegocioPosicion auxNegocioPosicion = new NegocioPosicion();
        auxNegocioPosicion.insertarPosicion(posicion);
    }
    
    @WebMethod(operationName = "eliminaPosicion")
    public void eliminaPosicion(@WebParam(name = "posicion") int id_posicion) {
        NegocioPosicion auxNegocioPosicion = new NegocioPosicion();
        auxNegocioPosicion.eliminaPosicion(id_posicion);
    }
    
    @WebMethod(operationName = "actualizaPosicion")
    public void actualizaPosicion(@WebParam(name = "posicion") Posicion posicion) {
        NegocioPosicion auxNegocioPosicion = new NegocioPosicion();
        auxNegocioPosicion.actualizaPosicion(posicion);
    }
    
    @WebMethod(operationName = "insertaTipoPartido")
    public void insertaTipoPartido(@WebParam(name = "tipoPartido") TipoPartido tipoPartido) {
        NegocioTipoPartido auxNegocioTipoPartido = new NegocioTipoPartido();
        auxNegocioTipoPartido.insertarTipoPartido(tipoPartido);
    }
    
    @WebMethod(operationName = "actualizaTipoPartido")
    public void actualizaTipoPartido(@WebParam(name = "tipoPartido") TipoPartido tipoPartido) {
        NegocioTipoPartido auxNegocioTipoPartido = new NegocioTipoPartido();
        auxNegocioTipoPartido.actualizaTipoPartido(tipoPartido);
    }
    
    @WebMethod(operationName = "eliminaTipoPartido")
    public void eliminaTipoPartido(@WebParam(name = "id_tipo_partido") int id_tipo_partido) {
        NegocioTipoPartido auxNegocioTipoPartido = new NegocioTipoPartido();
        auxNegocioTipoPartido.eliminarTipoPartido(id_tipo_partido);
    }
    
    @WebMethod(operationName = "buscarTipoPartido")
    public TipoPartido buscarTipoPartido(@WebParam(name = "descripcion") String descripcion) {
        NegocioTipoPartido auxNegocioTipoPartido = new NegocioTipoPartido();
        TipoPartido auxTipoPartido = new TipoPartido();
        auxTipoPartido = auxNegocioTipoPartido.buscarTipoPartido(descripcion);
        return auxTipoPartido;
    }
    
    @WebMethod(operationName = "listarTipoPartidos")
    public ArrayList<TipoPartido> listarTipoPartidos() {
        NegocioTipoPartido auxNegocioTipoPartido = new NegocioTipoPartido();
        ArrayList<TipoPartido> auxListaTipoPartidos = new ArrayList<>();
        auxListaTipoPartidos = auxNegocioTipoPartido.listaTipoPartidos();
        return auxListaTipoPartidos;
    }
    
    @WebMethod(operationName = "insertarCancha")
    public void insertarCancha(@WebParam(name = "cancha") Cancha cancha) {
        NegocioCancha auxNegocioCancha = new NegocioCancha();
        auxNegocioCancha.insertarCancha(cancha);
    }
    
    @WebMethod(operationName = "actualizaCancha")
    public void actualizaCancha(@WebParam(name = "tipoPartido") Cancha cancha) {
        NegocioCancha auxNegocioCancha = new NegocioCancha();
        auxNegocioCancha.actualizarCancha(cancha);
    }
    
    @WebMethod(operationName = "eliminarCancha")
    public void eliminarCancha(@WebParam(name = "id_cancha") int id_cancha) {
        NegocioCancha auxNegocioCancha = new NegocioCancha();
        auxNegocioCancha.eliminarCancha(id_cancha);
    }
    
    @WebMethod(operationName = "buscarCanchaPorNombre")
    public Cancha buscarCanchaPorNombre(@WebParam(name = "nombre_cancha") String nombre_cancha) {
        NegocioCancha auxNegocioCancha = new NegocioCancha();
        Cancha auxCancha = new Cancha();
        auxCancha = auxNegocioCancha.buscarCancha(nombre_cancha);
        return auxCancha;
    }
    
    @WebMethod(operationName = "listarCanchas")
    public ArrayList<Cancha> listarCanchas() {
        NegocioCancha auxNegocioCancha = new NegocioCancha();
        ArrayList<Cancha> auxListarCanchas = new ArrayList<>();
        auxListarCanchas = auxNegocioCancha.listarCanchas();
        return auxListarCanchas;
    }
    

}
